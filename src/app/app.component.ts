import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {ModalsService} from './services/modals/modals.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // Конструктор класса
  constructor(
      private router: Router,
      public modalsService: ModalsService
  ) {
  }
  // Метод, проверяющий является ли путь мастер слоем
  public isMasterLayout() {
    return !this.router.url.startsWith('/admin');
  }
}
