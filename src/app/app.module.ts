import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';


import { AppComponent } from './app.component';
import {AppRoutes} from './routing/routing';
import {MasterModule} from './pages/master/master.module';
import {RouterModule} from '@angular/router';
import {MasterLayoutModule} from './layouts/master-layout/master-layout.module';
import {AdminLayoutModule} from './layouts/admin-layout/admin-layout.module';
import {AdminModule} from './pages/admin/admin.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {RequestsService} from './services/requests/requests.service';
import {UserService} from './services/user/user.service';
import {IsAuthGuard} from './guards/is-auth/is-auth.guard';
import {IsAdminGuard} from './guards/is-admin/is-admin.guard';
import { ModalDonateComponent } from './modals/modal-donate/modal-donate.component';
import {ModalsService} from './services/modals/modals.service';
import { ModalAddressComponent } from './modals/modal-address/modal-address.component';
import { ModalPromoCodeComponent } from './modals/modal-promo-code/modal-promo-code.component';
import { ModalPrizeComponent } from './modals/modal-prize/modal-prize.component';
import {GameInitedGuard} from './guards/game-inited/game-inited.guard';
import {WebSocketService} from './services/websocket/web-socket.service';
import {FormsModule} from '@angular/forms';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { ModalTrackCodeComponent } from './modals/modal-track-code/modal-track-code.component';
import {ModalPrizeDialogComponent} from './modals/modal-prize/modal-prize-dialog/modal-prize-dialog';
import {BoxesService} from './services/boxes/boxes.service';

export function initBoxesService(boxesService: BoxesService) {
  return () => boxesService.isInited;
}

@NgModule({
  declarations: [
    AppComponent,
    ModalDonateComponent,
    ModalAddressComponent,
    ModalPromoCodeComponent,
    ModalPrizeComponent,
    ModalTrackCodeComponent,
    ModalPrizeDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes),
    HttpClientModule,
    AdminModule,
    MasterModule,
    MasterLayoutModule,
    AdminLayoutModule,
    FormsModule,
    MatCheckboxModule,
    MatButtonModule
  ],
  providers: [
    RequestsService,
    WebSocketService,
    UserService,
    ModalsService,
    IsAuthGuard,
    IsAdminGuard,
    GameInitedGuard,

    { provide: APP_INITIALIZER, useFactory: initBoxesService, deps: [BoxesService], multi: true },
  ],
  entryComponents: [
    ModalPrizeDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
