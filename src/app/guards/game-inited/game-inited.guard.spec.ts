import { TestBed, async, inject } from '@angular/core/testing';

import { GameInitedGuard } from './game-inited.guard';

describe('GameInitedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameInitedGuard]
    });
  });

  it('should ...', inject([GameInitedGuard], (guard: GameInitedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
