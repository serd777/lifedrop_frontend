import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {BoxesService} from '../../services/boxes/boxes.service';
import {OwnBoxesService} from '../../services/own_boxes/own-boxes.service';

@Injectable()
export class GameInitedGuard implements CanActivate {
  constructor(
    private boxesService: BoxesService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise<boolean>((resolve, reject) => {
      let counter = 0; // Счетчик кол-ва попыток

      const checkF = () => {
        if (++counter === 10) {
          this.router.navigateByUrl('/');
          resolve(false);
        }

        if (this.boxesService.isInited) {
          resolve(true);
        } else {
          setTimeout(checkF, 1000);
        }
      };

      checkF();
    });
  }
}
