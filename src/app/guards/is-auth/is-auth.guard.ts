import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {UserService} from '../../services/user/user.service';

@Injectable()
export class IsAuthGuard implements CanActivate {
  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise<boolean>((resolve, reject) => {
      const checkF = () => {
        if (this.userService.isInited) {
          if (!this.userService.isAuth) { this.router.navigate(['/']); }
          resolve(this.userService.isAuth);
        } else {
          setTimeout(checkF, 1000);
        }
      };

      checkF();
    });
  }
}

