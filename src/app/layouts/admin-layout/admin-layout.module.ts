import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminLayoutComponent} from './admin-layout.component';
import {RouterModule} from '@angular/router';
import {AppRoutes} from '../../routing/routing';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { AdminWrapComponent } from './admin-wrap/admin-wrap.component';
import { AdminTopBarComponent } from './admin-top-bar/admin-top-bar.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AppRoutes)
  ],
  exports: [
    AdminLayoutComponent
  ],
  declarations: [
    AdminLayoutComponent,
    AdminMenuComponent,
    AdminWrapComponent,
    AdminTopBarComponent
  ]
})
export class AdminLayoutModule { }
