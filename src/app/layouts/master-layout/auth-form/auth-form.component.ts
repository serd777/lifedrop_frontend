import { Component, OnInit } from '@angular/core';
import {BackendRoutes} from '../../../routing/pathes';

@Component({
  selector: 'app-master-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.css']
})
export class MasterAuthFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {}
  getAuthUrl() { return BackendRoutes.AuthVk; }
}
