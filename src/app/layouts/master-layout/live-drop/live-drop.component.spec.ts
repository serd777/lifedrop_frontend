import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveDropComponent } from './live-drop.component';

describe('LiveDropComponent', () => {
  let component: LiveDropComponent;
  let fixture: ComponentFixture<LiveDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
