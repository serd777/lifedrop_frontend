import { Component, OnInit } from '@angular/core';
import {LiveDropService} from '../../../services/live_drop/live-drop.service';
import {Drop} from '../../../models/drop';
import {BackendHost} from '../../../routing/pathes';

@Component({
  selector: 'app-master-livedrop',
  templateUrl: './live-drop.component.html',
  styleUrls: ['./live-drop.component.css']
})
export class MasterLiveDropComponent implements OnInit {

  constructor(
    public liveDropService: LiveDropService
  ) { }

  ngOnInit() {}
  getImg(drop: Drop) {
    return BackendHost + drop.item.img_path;
  }

}
