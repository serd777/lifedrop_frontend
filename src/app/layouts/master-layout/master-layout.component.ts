import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {ModalNames, ModalsService} from '../../services/modals/modals.service';

@Component({
  selector: 'app-master-layout',
  templateUrl: './master-layout.component.html',
  styleUrls: ['./master-layout.component.css']
})
export class MasterLayoutComponent implements OnInit {

  constructor(
    public usersService: UserService,
    private modalsService: ModalsService
  ) { }

  ngOnInit() {}
  openDonateModal() {
    this.modalsService.showModal(ModalNames.DonateModal);
  }
}
