import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MasterNavigationComponent} from './navigation/navigation.component';
import {MasterStatisticComponent} from './statistic/statistic.component';
import {MasterLayoutComponent} from './master-layout.component';
import {RouterModule} from '@angular/router';
import {AppRoutes} from '../../routing/routing';
import { MasterProfileComponent } from './profile/profile.component';
import { MasterAuthFormComponent } from './auth-form/auth-form.component';
import { MasterLiveDropComponent } from './live-drop/live-drop.component';
import { MasterTopUsersComponent } from './top-users/top-users.component';
import {ReverseArrayPipe} from '../../pipes/master/reverse_array/reverse-array.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AppRoutes)
  ],
  exports: [
      MasterLayoutComponent
  ],
  providers: [],
  declarations: [
    MasterNavigationComponent,
    MasterStatisticComponent,
    MasterLayoutComponent,
    MasterProfileComponent,
    MasterAuthFormComponent,
    MasterLiveDropComponent,
    MasterTopUsersComponent,
    ReverseArrayPipe
  ]
})
export class MasterLayoutModule { }
