import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {BackendRoutes} from '../../../routing/pathes';
import {ModalNames, ModalsService} from '../../../services/modals/modals.service';

@Component({
  selector: 'app-master-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class MasterProfileComponent implements OnInit {

  constructor(
    public userService: UserService,
    private modalsService: ModalsService
  ) { }

  ngOnInit() {}

  getLogoutRoute() { return BackendRoutes.Logout; }
  showDonateModal() { this.modalsService.showModal(ModalNames.DonateModal); }
}
