import { Component, OnInit } from '@angular/core';
import {StatisticService} from '../../../services/statistic/statistic.service';

@Component({
  selector: 'app-master-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class MasterStatisticComponent implements OnInit {

  constructor(
    public statService: StatisticService
  ) { }

  ngOnInit() {
  }

}
