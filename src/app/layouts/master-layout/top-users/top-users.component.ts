import { Component, OnInit } from '@angular/core';
import {StatisticService} from '../../../services/statistic/statistic.service';

@Component({
  selector: 'app-master-topusers',
  templateUrl: './top-users.component.html',
  styleUrls: ['./top-users.component.css']
})
export class MasterTopUsersComponent implements OnInit {

  constructor(
    public statisticService: StatisticService
  ) { }

  ngOnInit() {}

}
