import { Component, OnInit } from '@angular/core';
import {ModalsService} from '../../services/modals/modals.service';
import {UserService} from '../../services/user/user.service';
import {ServerAnswer} from '../../services/requests/requests.service';

@Component({
  selector: 'app-modal-address',
  templateUrl: './modal-address.component.html',
  styleUrls: ['./modal-address.component.css']
})
export class ModalAddressComponent implements OnInit {
  public isFailed: boolean;
  public isSuccess: boolean;
  public errMsg: string;

  constructor(
    public modalsService: ModalsService,
    public userService: UserService
  ) {
    this.isFailed  = false;
    this.isSuccess = false;
    this.errMsg    = '';
  }

  ngOnInit() {}
  updateAddress() {
    this.isFailed  = false;
    this.isSuccess = false;

    this.userService.updateAddress()
      .then(() => this.isSuccess = true)
      .catch((answer: ServerAnswer) => {
        this.errMsg = answer.data;
        this.isFailed = true;
      });
  }
}
