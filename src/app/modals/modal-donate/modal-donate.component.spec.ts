import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDonateComponent } from './modal-donate.component';

describe('ModalDonateComponent', () => {
  let component: ModalDonateComponent;
  let fixture: ComponentFixture<ModalDonateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDonateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDonateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
