import { Component, OnInit } from '@angular/core';
import {ModalsService} from '../../services/modals/modals.service';
import {BackendRoutes} from '../../routing/pathes';

@Component({
  selector: 'app-modal-donate',
  templateUrl: './modal-donate.component.html',
  styleUrls: ['./modal-donate.component.css']
})
export class ModalDonateComponent implements OnInit {
  constructor(
    public modalsService: ModalsService
  ) { }

  ngOnInit() {}
  getUrl() {
    return BackendRoutes.PaymentGateway;
  }
}
