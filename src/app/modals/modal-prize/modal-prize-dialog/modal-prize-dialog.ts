import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-modals-prize-dialog',
  templateUrl: './modal-prize-dialog.html',
  styleUrls: ['./modal-prize-dialog.css']
})
export class ModalPrizeDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ModalPrizeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
