import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPrizeComponent } from './modal-prize.component';

describe('ModalPrizeComponent', () => {
  let component: ModalPrizeComponent;
  let fixture: ComponentFixture<ModalPrizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPrizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPrizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
