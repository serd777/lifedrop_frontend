import { Component, OnInit } from '@angular/core';
import {ModalsService} from '../../services/modals/modals.service';
import {GameService} from '../../services/game/game.service';
import {Item} from '../../models/item';
import {BackendHost} from '../../routing/pathes';
import {MatDialog} from '@angular/material';
import {ModalPrizeDialogComponent} from './modal-prize-dialog/modal-prize-dialog';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-modal-prize',
  templateUrl: './modal-prize.component.html',
  styleUrls: ['./modal-prize.component.css']
})
export class ModalPrizeComponent implements OnInit {

  constructor(
    public modalsService: ModalsService,
    public gameService: GameService,
    private userService: UserService,
    public dialogPrize: MatDialog,
  ) { }

  ngOnInit() {}
  getImg(item: Item) {
    return BackendHost + item.img_path;
  }

  sellItem() {
    this.gameService.sellItem(this.gameService.prize);
    this.modalsService.hideAll();
  }
  takeItem() {
    if (!this.gameService.isPrizeFromGame) {
      if (this.userService.userAddress.name === '') {
        this.showAddressError();
        return;
      }

      this.takePrize();
      return;
    }

    this.modalsService.hideAll();
    /*const dialogRef = this.dialogPrize.open(
      ModalPrizeDialogComponent,
      {
        width: '250px',
        data: {msg: 'Предмет перемещен во вкладку "Мои призы" в Личном Кабинете', color: 'green'}
      });*/
  }

  takePrize() {
    this.gameService.takeItem(this.gameService.prize);
    this.modalsService.hideAll();
  }
  showAddressError() {
    this.modalsService.hideAll();
    const dialogRef = this.dialogPrize.open(
      ModalPrizeDialogComponent,
      {
        width: '250px',
        data: {msg: 'Вы должны сначала заполнить адрес в вашем профиле', color: 'red'}
      });
  }
}
