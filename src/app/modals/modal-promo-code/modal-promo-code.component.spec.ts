import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPromoCodeComponent } from './modal-promo-code.component';

describe('ModalPromoCodeComponent', () => {
  let component: ModalPromoCodeComponent;
  let fixture: ComponentFixture<ModalPromoCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPromoCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPromoCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
