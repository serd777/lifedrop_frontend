import { Component, OnInit } from '@angular/core';
import {ModalsService} from '../../services/modals/modals.service';
import {PromoService} from '../../services/promo/promo.service';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-modal-promo-code',
  templateUrl: './modal-promo-code.component.html',
  styleUrls: ['./modal-promo-code.component.css']
})
export class ModalPromoCodeComponent implements OnInit {
  public promoCode: string;
  public activateFailed: boolean;
  public activatedSuccess: boolean;

  constructor(
    public modalsService: ModalsService,
    private promoService: PromoService,
    private userService: UserService
  ) {
    this.promoCode = '';
    this.activateFailed = false;
    this.activatedSuccess = false;
  }

  ngOnInit() {}
  activatePromo() {
    this.promoService.activatePromo(this.promoCode)
      .then((amount: number) => {
        this.userService.user.balance += amount;
        this.activatedSuccess = true;
        this.activateFailed = false;
      })
      .catch(() => {
        this.activateFailed = true;
        this.activatedSuccess = false;
      });
  }
}
