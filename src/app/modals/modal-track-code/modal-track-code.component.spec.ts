import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTrackCodeComponent } from './modal-track-code.component';

describe('ModalTrackCodeComponent', () => {
  let component: ModalTrackCodeComponent;
  let fixture: ComponentFixture<ModalTrackCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTrackCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTrackCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
