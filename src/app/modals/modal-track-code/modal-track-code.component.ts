import { Component, OnInit } from '@angular/core';
import {ModalsService} from '../../services/modals/modals.service';
import {GameService} from '../../services/game/game.service';

@Component({
  selector: 'app-modal-track-code',
  templateUrl: './modal-track-code.component.html',
  styleUrls: ['./modal-track-code.component.css']
})
export class ModalTrackCodeComponent implements OnInit {

  constructor(
    public modalsService: ModalsService,
    public gameService: GameService
  ) { }

  ngOnInit() {}

}
