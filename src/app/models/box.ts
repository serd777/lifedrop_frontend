import {BoxSection} from './box_section';

//  Модель игрового кейса
export class Box {
  constructor(
    public id:            number = 0,
    public name:          string = '',
    public img_path:      string = '',
    public price:         number = 0,
    public is_enabled:    boolean = false,
    public section:       undefined|BoxSection = undefined,
    public section_id:    number = 0,
    public img:           undefined|string = undefined,
    public color_num:     number = 0,
    public pos:           number = 0,
    public border_color:  string = '#000000',
    public is_priority:   boolean = false
  ) {}
}
