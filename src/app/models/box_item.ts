import {Box} from './box';
import {Item} from './item';

export class BoxItem {
  constructor(
    public id: number = 0,
    public box_id: number = 0,
    public item_id: number = 0,
    public percent: number = 0,
    public box: undefined|Box = undefined,
    public item: undefined|Item = undefined
  ) {}
}
