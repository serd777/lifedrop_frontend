//  Модель подкрутки
import {Item} from './item';

export class Cheat {
  constructor(
    public id: number = 0,
    public item_id: number = 0,
    public user_id: number = 0,
    public amount: number = 0,
    public item: Item|undefined = undefined
  ) {}
}
