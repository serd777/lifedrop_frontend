import {Item} from './item';
import {User} from './user';

//  Модель дропа пользователя
export class Drop {
  constructor(
    public id: number = 0,
    public item: undefined|Item = undefined,
    public user: undefined|User = undefined,
    public status: string = '',
    public track_code: string = '',
    public created_at: string = '',
    public item_id: number = 0,
    public user_id: number = 0,
    public border_color: string = '#FFFFFF'
  ) {}
}
