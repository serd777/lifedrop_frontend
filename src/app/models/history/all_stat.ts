import {OwnBoxesStat} from './own_boxes_stat';
import {PaymentStat} from './payment_stat';
import {ReferralStat} from './referral_stat';
import {PromoStat} from './promo_stat';
import {Drop} from '../drop';

export class AllUserStat {
  constructor(
    public own_box_stat:    Array<OwnBoxesStat> = [],
    public payment_stat:    Array<PaymentStat>  = [],
    public referral_stat:   Array<ReferralStat> = [],
    public promo_stat:      Array<PromoStat>    = [],
    public drops_stat:      Array<Drop>         = [],
    public referrals_count: number              = 0
  ) {}
}
