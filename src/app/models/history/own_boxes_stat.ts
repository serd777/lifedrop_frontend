import {User} from '../user';
import {OwnBox} from '../own_box';

export class OwnBoxesStat {
  constructor(
    public id: number = 0,
    public mentor_id: number = 0,
    public user_id: number = 0,
    public box_id: number = 0,
    public amount: number = 0,
    public mentor: User|undefined = undefined,
    public user: User|undefined = undefined,
    public box: OwnBox|undefined = undefined,
    public created_at: string = ''
  ) {}
}
