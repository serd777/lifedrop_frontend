import {User} from '../user';

export class PaymentStat {
  constructor(
    public id: number = 0,
    public user_id: number = 0,
    public amount: number = 0,
    public user: User|undefined = undefined,
    public created_at: string = ''
  ) {}
}
