import {User} from '../user';
import {PromoCode} from '../promo_code';

export class PromoStat {
  constructor(
    public id: number = 0,
    public user_id: number = 0,
    public code_id: number = 0,
    public user: User|undefined = undefined,
    public code: PromoCode|undefined = undefined,
    public created_at: string = ''
  ) {}
}
