import {User} from '../user';

export class ReferralStat {
  constructor(
    public id: number = 0,
    public mentor_id: number = 0,
    public user_id: number = 0,
    public amount: number = 0,
    public mentor: User|undefined = undefined,
    public user: User|undefined = undefined,
    public created_at: string = ''
  ) {}
}
