//  Модель игрового предмета
export class Item {
  constructor(
    public id:       number = 0,
    public name:     string = '',
    public img_path: string = '',
    public price:    number = 0,
    public img:      undefined|string = undefined,
  ) {}
}
