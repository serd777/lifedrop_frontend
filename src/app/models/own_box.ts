//  Модель собственного кейса
import {OwnBoxImg} from './own_box_img';

export class OwnBox {
  constructor(
    public id: number = 0,
    public name: string = '',
    public price: number = 0,
    public img: OwnBoxImg|undefined = undefined,
    public img_id: number = 0,
    public user_id: number = 0
  ) {}
}
