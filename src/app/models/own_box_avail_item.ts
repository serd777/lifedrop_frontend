//  Модель доступного предмета

import {Item} from './item';

export class OwnBoxAvailItem {
  constructor(
    public id: number = 0,
    public item: Item|undefined = undefined,
    public item_id: number = 0
  ) {}
}
