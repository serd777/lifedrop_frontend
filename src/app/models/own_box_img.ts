//  Модель изображения кейса юзера
export class OwnBoxImg {
  constructor(
    public id: number = 0,
    public img_path: string = ''
  ) {}
}
