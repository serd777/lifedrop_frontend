//  Модель предмета кейса юзера

import {Item} from './item';

export class OwnBoxItem {
  constructor(
    public id: number = 0,
    public item: Item|undefined = undefined,
    public item_id: number = 0,
    public box_id: number = 0
  ) {}
}
