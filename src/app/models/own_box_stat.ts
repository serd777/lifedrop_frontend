//  OwnBox Stat
export class OwnBoxStat {
  constructor(
    public box_id: number = 0,
    public amount: number = 0
  ) {}
}
