//  Модель промо кода
export class PromoCode {
  constructor(
    public id: number = 0,
    public code: string = '',
    public amount: number = 0,
    public price: number = 0
  ) {}
}
