//  Модель промо-акции

export class PromoOffer {
  constructor(
    public id: number = 0,
    public title: string = '',
    public about: string = '',
    public pos: number = 0,
    public img_path: string = '',
    public img: undefined|string = undefined
  ) {}
}
