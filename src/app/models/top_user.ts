//  Модель Топ Юзера

import {User} from './user';

export class TopUser {
  constructor(
    public id:      number = 0,
    public user:    User|undefined = undefined,
    public user_id: number = 0,
    public games:   number = 0,
    public profit:  number = 0
  ) {}
}
