// Модель пользователя
export class User {
  constructor(
    public id:         number = 0,
    public name:       string = '',
    public avatar:     string = '',
    public vk_id:      string = '',
    public balance:    number = 0,
    public is_blocked: boolean = false,
    public is_admin:   boolean = false
  ) {}
}
