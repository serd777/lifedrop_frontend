//  Модель адреса пользователя

export class UserAddress {
  constructor(
    public id: number = 0,
    public name: string = '',
    public country: string = '',
    public region: string = '',
    public city: string = '',
    public address: string = '',
    public post_index: string = '',
    public has_post_index: boolean = false,
    public phone: string = '',
    public additional_info: string = ''
  ) {}
}
