import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminIndexPageComponent } from './index-page/index-page.component';
import { AdminUsersPageComponent } from './users-page/users-page.component';
import { AdminItemsPageComponent } from './items-page/items-page.component';
import { AdminCasesPageComponent } from './cases-page/cases-page.component';
import { AdminPromoCodesComponent } from './promo-codes/promo-codes.component';
import { AdminPayoutsPageComponent } from './payouts-page/payouts-page.component';
import { AdminPromoOffersComponent } from './promo-offers/promo-offers.component';
import { AdminStatisticService } from '../../services/admin/statistic/admin-statistic.service';
import {AdminUsersService} from '../../services/admin/users/admin-users.service';
import {NgxPaginationModule} from 'ngx-pagination';
import {AdminUserNameFilterPipe} from '../../pipes/admin/user-name/admin-user-name-filter.pipe';
import { NewItemComponent } from './items-page/new-item/new-item.component';
import { ItemsListComponent } from './items-page/items-list/items-list.component';
import {AdminItemsService} from '../../services/admin/items/admin-items.service';
import {AdminItemNamePipe} from '../../pipes/admin/item-name/admin-item-name.pipe';
import { UsersListComponent } from './users-page/users-list/users-list.component';
import { PromoOffersListComponent } from './promo-offers/promo-offers-list/promo-offers-list.component';
import { NewPromoOfferComponent } from './promo-offers/new-promo-offer/new-promo-offer.component';
import {AdminPromoOffersService} from '../../services/admin/promo_offers/admin-promo-offers.service';
import { PromoCodesListComponent } from './promo-codes/promo-codes-list/promo-codes-list.component';
import { NewPromoCodeComponent } from './promo-codes/new-promo-code/new-promo-code.component';
import {AdminPromoCodesService} from '../../services/admin/promo_codes/admin-promo-codes.service';
import {
  MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatSelectModule,
  MatTabsModule
} from '@angular/material';
import { NewBoxSectionComponent } from './cases-page/new-box-section/new-box-section.component';
import {AdminBoxesSectionsService} from '../../services/admin/boxes_sections/admin-boxes-sections.service';
import { BoxesSectionsListComponent } from './cases-page/boxes-sections-list/boxes-sections-list.component';
import { UserInfoComponent } from './users-page/user-info/user-info.component';
import { ItemInfoComponent } from './items-page/item-info/item-info.component';
import { AdminPromoOfferInfoComponent } from './promo-offers/promo-offer-info/promo-offer-info.component';
import { NewBoxComponent } from './cases-page/new-box/new-box.component';
import { BoxesListComponent } from './cases-page/boxes-list/boxes-list.component';
import {AdminBoxesService} from '../../services/admin/boxes/admin-boxes.service';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import { NewBoxItemComponent } from './cases-page/new-box-item/new-box-item.component';
import { BoxItemsListComponent } from './cases-page/box-items-list/box-items-list.component';
import { EditBoxComponent } from './cases-page/edit-box/edit-box.component';
import {AdminPayoutsService} from '../../services/admin/payouts/admin-payouts.service';
import { AdminWaitingPayoutsComponent } from './payouts-page/waiting-payouts/waiting-payouts.component';
import { AdminCollectingPayoutsComponent } from './payouts-page/collecting-payouts/collecting-payouts.component';
import { AdminSentPayoutsComponent } from './payouts-page/sent-payouts/sent-payouts.component';
import { AdminTrackCodeDialogComponent } from './payouts-page/track-code-dialog/track-code-dialog';
import {CdkTableModule} from '@angular/cdk/table';
import { AdminNewOwnBoxImgComponent } from './cases-page/new-own-box-img/new-own-box-img.component';
import {AdminOwnBoxesService} from '../../services/admin/own_boxes/admin-own-boxes.service';
import { AdminOwnImgListComponent } from './cases-page/own-img-list/own-img-list.component';
import {AdminCommentDialogComponent} from './payouts-page/comment-dialog/comment-dialog';
import { AdminUserCheatsComponent } from './users-page/user-cheats/user-cheats.component';
import {AdminAddressDialogComponent} from './payouts-page/address-dialog/address-dialog';
import { AdminOwnItemsListComponent } from './cases-page/own-items-list/own-items-list.component';
import { AdminNewOwnBoxItemComponent } from './cases-page/new-own-box-item/new-own-box-item.component';
import { AdminUserPaymentHistoryComponent } from './users-page/user-info/user-payment-history/user-payment-history.component';
import { AdminUserPromoHistoryComponent } from './users-page/user-info/user-promo-history/user-promo-history.component';
import { AdminUserReferralHistoryComponent } from './users-page/user-info/user-referral-history/user-referral-history.component';
import { AdminUserOwnBoxHistoryComponent } from './users-page/user-info/user-own-box-history/user-own-box-history.component';
import { AdminUserBoxesHistoryComponent } from './users-page/user-info/user-boxes-history/user-boxes-history.component';
import { AdminDonateGraphComponent } from './index-page/donate-graph/donate-graph.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    NgxPaginationModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    CdkTableModule,
    NgxChartsModule
  ],
  providers: [
    AdminStatisticService,
    AdminUsersService,
    AdminItemsService,
    AdminPromoOffersService,
    AdminPromoCodesService,
    AdminBoxesSectionsService,
    AdminBoxesService,
    AdminPayoutsService,
    AdminOwnBoxesService
  ],
  declarations: [
    AdminIndexPageComponent,
    AdminUsersPageComponent,
    AdminItemsPageComponent,
    AdminCasesPageComponent,
    AdminPromoCodesComponent,
    AdminPayoutsPageComponent,
    AdminPromoOffersComponent,
    AdminUserNameFilterPipe,
    AdminItemNamePipe,
    NewItemComponent,
    ItemsListComponent,
    UsersListComponent,
    PromoOffersListComponent,
    NewPromoOfferComponent,
    PromoCodesListComponent,
    NewPromoCodeComponent,
    NewBoxSectionComponent,
    BoxesSectionsListComponent,
    UserInfoComponent,
    ItemInfoComponent,
    AdminPromoOfferInfoComponent,
    NewBoxComponent,
    BoxesListComponent,
    NewBoxItemComponent,
    BoxItemsListComponent,
    EditBoxComponent,
    AdminWaitingPayoutsComponent,
    AdminCollectingPayoutsComponent,
    AdminSentPayoutsComponent,
    AdminTrackCodeDialogComponent,
    AdminNewOwnBoxImgComponent,
    AdminOwnImgListComponent,
    AdminCommentDialogComponent,
    AdminAddressDialogComponent,
    AdminUserCheatsComponent,
    AdminOwnItemsListComponent,
    AdminNewOwnBoxItemComponent,
    AdminUserPaymentHistoryComponent,
    AdminUserPromoHistoryComponent,
    AdminUserReferralHistoryComponent,
    AdminUserOwnBoxHistoryComponent,
    AdminUserBoxesHistoryComponent,
    AdminDonateGraphComponent
  ],
  entryComponents: [
    AdminTrackCodeDialogComponent,
    AdminCommentDialogComponent,
    AdminAddressDialogComponent
  ]
})
export class AdminModule { }
