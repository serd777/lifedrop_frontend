import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxItemsListComponent } from './box-items-list.component';

describe('BoxItemsListComponent', () => {
  let component: BoxItemsListComponent;
  let fixture: ComponentFixture<BoxItemsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxItemsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxItemsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
