import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Box} from '../../../../models/box';
import {BoxItem} from '../../../../models/box_item';
import {AdminBoxesService} from '../../../../services/admin/boxes/admin-boxes.service';

@Component({
  selector: 'app-admin-box-items-list',
  templateUrl: './box-items-list.component.html',
  styleUrls: ['./box-items-list.component.css']
})
export class BoxItemsListComponent implements OnInit {
  private _box: Box;
  private _newItem: BoxItem;

  public boxItems: Array<BoxItem>;
  public page: number;

  constructor(
    private boxesService: AdminBoxesService
  ) {
    this.page = 1;
    this.boxItems = [];
  }

  ngOnInit() {}

  removeItem(item: BoxItem) {
    this.boxesService.deleteBoxItem(item);

    const index = this.boxItems.indexOf(item);
    this.boxItems.splice(index, 1);
  }
  updateItem(item: BoxItem) {
    this.boxesService.updateDropPercent(item);
  }

  get box() { return this._box; }
  @Input() set box(value: Box) {
    this._box = value;
    this.boxesService.getBoxItems(this._box)
      .then((items: Array<BoxItem>) => {
        this.boxItems = items;
      });
  }

  get newItem() { return this._newItem; }
  @Input() set newItem(value: BoxItem) {
    if (value !== undefined) {
      this.boxItems.push(value);
    }

    this._newItem = value;
  }
}
