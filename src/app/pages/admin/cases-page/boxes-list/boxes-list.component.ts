import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AdminBoxesService} from '../../../../services/admin/boxes/admin-boxes.service';
import {Box} from '../../../../models/box';

@Component({
  selector: 'app-admin-boxes-list',
  templateUrl: './boxes-list.component.html',
  styleUrls: ['./boxes-list.component.css']
})
export class BoxesListComponent implements OnInit {
  @Output() onBoxSelected = new EventEmitter<Box>();
  @Output() onBoxEdit = new EventEmitter<Box>();

  public page: number;

  constructor(
    public boxesService: AdminBoxesService
  ) {
    this.page = 1;
  }

  ngOnInit() {}
  selectBox(box: Box) {
    this.onBoxSelected.emit(box);
  }
  editBox(box: Box) {
    this.onBoxEdit.emit(box);
  }
}
