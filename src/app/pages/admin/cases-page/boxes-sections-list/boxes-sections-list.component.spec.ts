import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxesSectionsListComponent } from './boxes-sections-list.component';

describe('BoxesSectionsListComponent', () => {
  let component: BoxesSectionsListComponent;
  let fixture: ComponentFixture<BoxesSectionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxesSectionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxesSectionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
