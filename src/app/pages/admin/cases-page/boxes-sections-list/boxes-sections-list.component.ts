import { Component, OnInit } from '@angular/core';
import {AdminBoxesSectionsService} from '../../../../services/admin/boxes_sections/admin-boxes-sections.service';
import {BoxSection} from '../../../../models/box_section';

@Component({
  selector: 'app-admin-boxes-sections-list',
  templateUrl: './boxes-sections-list.component.html',
  styleUrls: ['./boxes-sections-list.component.css']
})
export class BoxesSectionsListComponent implements OnInit {
  public page: number;

  constructor(
    public sectionsService: AdminBoxesSectionsService
  ) {
    this.page = 1;
  }

  ngOnInit() {}
}
