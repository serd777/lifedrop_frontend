import { Component, OnInit } from '@angular/core';
import {Box} from '../../../models/box';
import {BoxItem} from '../../../models/box_item';

@Component({
  selector: 'app-admin-cases-page',
  templateUrl: './cases-page.component.html',
  styleUrls: ['./cases-page.component.css']
})
export class AdminCasesPageComponent implements OnInit {
  public selectedBox: Box;
  public editableBox: Box;
  public newBoxItem: BoxItem;

  public caseItemsShowed: boolean;
  public casesListShowed: boolean;
  public editCaseShowed:  boolean;

  constructor() {
    this.caseItemsShowed = false;
    this.casesListShowed = true;
    this.editCaseShowed  = false;
  }

  ngOnInit() {}
  selectBox(box: Box) {
    this.selectedBox = box;

    this.caseItemsShowed = true;
    this.casesListShowed = false;
  }
  editBox(box: Box) {
    this.editableBox = box;

    this.editCaseShowed = true;
    this.casesListShowed = false;
  }
  returnToList() {
    this.casesListShowed = true;
    this.caseItemsShowed = false;
    this.editCaseShowed  = false;
  }
}
