import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Box} from '../../../../models/box';
import {AdminBoxesSectionsService} from '../../../../services/admin/boxes_sections/admin-boxes-sections.service';
import {AdminBoxesService} from '../../../../services/admin/boxes/admin-boxes.service';

@Component({
  selector: 'app-admin-edit-box',
  templateUrl: './edit-box.component.html',
  styleUrls: ['./edit-box.component.css']
})
export class EditBoxComponent implements OnInit {
  private _box: Box;
  @Output() onReturnEmitted = new EventEmitter<boolean>();

  constructor(
    public sectionsService: AdminBoxesSectionsService,
    public boxesService: AdminBoxesService
  ) {
    this._box = new Box();
  }

  ngOnInit() {}
  parseImg(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  updateBox() {
    this.boxesService.updateBox(this._box);
    this.onReturnEmitted.emit(true);
  }
  returnBack() {
    this.onReturnEmitted.emit(true);
  }

  get box() { return this._box; }
  @Input() set box(value: Box) {
    this._box = value;
  }

  private _handleReaderLoaded(evt) {
    const binaryString = evt.target.result;
    this._box.img = btoa(binaryString);
  }
}
