import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBoxItemComponent } from './new-box-item.component';

describe('NewBoxItemComponent', () => {
  let component: NewBoxItemComponent;
  let fixture: ComponentFixture<NewBoxItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBoxItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBoxItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
