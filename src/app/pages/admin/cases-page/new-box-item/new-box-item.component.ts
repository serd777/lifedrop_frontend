import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AdminItemsService} from '../../../../services/admin/items/admin-items.service';
import {Box} from '../../../../models/box';
import {BoxItem} from '../../../../models/box_item';
import {AdminBoxesService} from '../../../../services/admin/boxes/admin-boxes.service';

@Component({
  selector: 'app-admin-new-box-item',
  templateUrl: './new-box-item.component.html',
  styleUrls: ['./new-box-item.component.css']
})
export class NewBoxItemComponent implements OnInit, OnChanges {
  @Input() box: Box;
  @Output() onItemAdded = new EventEmitter<BoxItem>();

  public boxItem: BoxItem;

  constructor(
    public itemsService: AdminItemsService,
    private boxesService: AdminBoxesService
  ) {
    this.boxItem = new BoxItem();
  }

  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges) {
    this.boxItem.box_id = changes.box.currentValue.id;
  }

  addItem() {
    this.boxesService.addBoxItem(this.boxItem)
      .then((item: BoxItem) => {
        this.onItemAdded.emit(item);
        this.boxItem.item_id = 0;
      })
      .catch(() => {});
  }
}
