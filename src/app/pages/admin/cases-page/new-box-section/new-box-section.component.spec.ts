import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBoxSectionComponent } from './new-box-section.component';

describe('NewBoxSectionComponent', () => {
  let component: NewBoxSectionComponent;
  let fixture: ComponentFixture<NewBoxSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBoxSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBoxSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
