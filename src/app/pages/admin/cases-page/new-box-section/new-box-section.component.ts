import { Component, OnInit } from '@angular/core';
import {BoxSection} from '../../../../models/box_section';
import {AdminBoxesSectionsService} from '../../../../services/admin/boxes_sections/admin-boxes-sections.service';

@Component({
  selector: 'app-admin-new-box-section',
  templateUrl: './new-box-section.component.html',
  styleUrls: ['./new-box-section.component.css']
})
export class NewBoxSectionComponent implements OnInit {
  public newSection: BoxSection;
  public page: number;

  constructor(
    private sectionsService: AdminBoxesSectionsService
  ) {
    this.newSection = new BoxSection();
    this.page = 1;
  }

  ngOnInit() {}
  addSection() {
    this.sectionsService.addSection(this.newSection);
    this.newSection.name = '';
  }

}
