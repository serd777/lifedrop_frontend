import { Component, OnInit } from '@angular/core';
import {Box} from '../../../../models/box';
import {AdminBoxesService} from '../../../../services/admin/boxes/admin-boxes.service';
import {AdminBoxesSectionsService} from '../../../../services/admin/boxes_sections/admin-boxes-sections.service';

@Component({
  selector: 'app-admin-new-box',
  templateUrl: './new-box.component.html',
  styleUrls: ['./new-box.component.css']
})
export class NewBoxComponent implements OnInit {
  public newBox: Box;

  constructor(
    public boxesService: AdminBoxesService,
    public sectionsService: AdminBoxesSectionsService
  ) {
    this.newBox = new Box();
  }

  ngOnInit() {}
  parseImg(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  addBox() {
    this.boxesService.addBox(this.newBox);

    this.newBox.name = '';
    this.newBox.price = 0;
  }

  private _handleReaderLoaded(evt) {
    const binaryString = evt.target.result;
    this.newBox.img = btoa(binaryString);
  }

}
