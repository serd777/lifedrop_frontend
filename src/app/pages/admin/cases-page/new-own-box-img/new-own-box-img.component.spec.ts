import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOwnBoxImgComponent } from './new-own-box-img.component';

describe('NewOwnBoxImgComponent', () => {
  let component: NewOwnBoxImgComponent;
  let fixture: ComponentFixture<NewOwnBoxImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewOwnBoxImgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewOwnBoxImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
