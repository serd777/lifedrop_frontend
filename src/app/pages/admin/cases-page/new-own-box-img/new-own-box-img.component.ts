import { Component, OnInit } from '@angular/core';
import {AdminOwnBoxesService} from '../../../../services/admin/own_boxes/admin-own-boxes.service';

@Component({
  selector: 'app-admin-new-own-box-img',
  templateUrl: './new-own-box-img.component.html',
  styleUrls: ['./new-own-box-img.component.css']
})
export class AdminNewOwnBoxImgComponent implements OnInit {
  private newImg: string;

  constructor(
    private boxesService: AdminOwnBoxesService
  ) {
    this.newImg = '';
  }

  ngOnInit() {}
  parseImg(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  addImage() {
    this.boxesService.addImage(this.newImg);
  }

  private _handleReaderLoaded(evt) {
    const binaryString = evt.target.result;
    this.newImg = btoa(binaryString);
  }
}
