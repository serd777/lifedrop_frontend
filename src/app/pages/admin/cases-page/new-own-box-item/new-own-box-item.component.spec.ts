import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOwnBoxItemComponent } from './new-own-box-item.component';

describe('NewOwnBoxItemComponent', () => {
  let component: NewOwnBoxItemComponent;
  let fixture: ComponentFixture<NewOwnBoxItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewOwnBoxItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewOwnBoxItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
