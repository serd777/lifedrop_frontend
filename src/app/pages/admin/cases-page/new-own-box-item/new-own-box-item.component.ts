import { Component, OnInit } from '@angular/core';
import {OwnBoxAvailItem} from '../../../../models/own_box_avail_item';
import {AdminOwnBoxesService} from '../../../../services/admin/own_boxes/admin-own-boxes.service';
import {AdminItemsService} from '../../../../services/admin/items/admin-items.service';

@Component({
  selector: 'app-admin-new-own-box-item',
  templateUrl: './new-own-box-item.component.html',
  styleUrls: ['./new-own-box-item.component.css']
})
export class AdminNewOwnBoxItemComponent implements OnInit {
  public newItem: OwnBoxAvailItem;

  constructor(
    private boxesService: AdminOwnBoxesService,
    public itemsService: AdminItemsService
  ) {
    this.newItem = new OwnBoxAvailItem();
  }

  ngOnInit() {}
  addItem() {
    this.boxesService.addAvailItem(this.newItem.item);
    this.newItem.item = undefined;
  }
}
