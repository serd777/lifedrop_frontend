import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnImgListComponent } from './own-img-list.component';

describe('OwnImgListComponent', () => {
  let component: OwnImgListComponent;
  let fixture: ComponentFixture<OwnImgListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnImgListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnImgListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
