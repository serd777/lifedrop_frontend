import { Component, OnInit } from '@angular/core';
import {AdminOwnBoxesService} from '../../../../services/admin/own_boxes/admin-own-boxes.service';
import {OwnBoxImg} from '../../../../models/own_box_img';
import {BackendHost} from '../../../../routing/pathes';

@Component({
  selector: 'app-admin-own-img-list',
  templateUrl: './own-img-list.component.html',
  styleUrls: ['./own-img-list.component.css']
})
export class AdminOwnImgListComponent implements OnInit {
  public page: number;

  constructor(
    public boxesService: AdminOwnBoxesService
  ) {
    this.page = 1;
  }

  ngOnInit() {}
  getImg(img: OwnBoxImg) {
    return BackendHost + img.img_path;
  }

}
