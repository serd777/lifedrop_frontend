import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnItemsListComponent } from './own-items-list.component';

describe('OwnItemsListComponent', () => {
  let component: OwnItemsListComponent;
  let fixture: ComponentFixture<OwnItemsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnItemsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnItemsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
