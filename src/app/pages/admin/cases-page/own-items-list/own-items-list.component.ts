import { Component, OnInit } from '@angular/core';
import {AdminOwnBoxesService} from '../../../../services/admin/own_boxes/admin-own-boxes.service';
import {Item} from '../../../../models/item';
import {BackendHost} from '../../../../routing/pathes';

@Component({
  selector: 'app-admin-own-items-list',
  templateUrl: './own-items-list.component.html',
  styleUrls: ['./own-items-list.component.css']
})
export class AdminOwnItemsListComponent implements OnInit {
  public page: number;

  constructor(
    public boxesService: AdminOwnBoxesService
  ) {
    this.page = 1;
  }

  ngOnInit() {}
  getImg(item: Item) {
    return BackendHost + item.img_path;
  }
}
