import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonateGraphComponent } from './donate-graph.component';

describe('DonateGraphComponent', () => {
  let component: DonateGraphComponent;
  let fixture: ComponentFixture<DonateGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonateGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
