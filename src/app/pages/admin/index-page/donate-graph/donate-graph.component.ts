import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-admin-donate-graph',
  templateUrl: './donate-graph.component.html',
  styleUrls: ['./donate-graph.component.css']
})
export class AdminDonateGraphComponent implements OnInit {
  private _donates: Array<any>;
  private _payouts: Array<any>;
  public graphData: Array<any>;

  constructor() {
    this.graphData = [];
    this._payouts = [];
    this._donates = [];
  }

  ngOnInit() {}
  private findDay(arr: Array<any>, day: number) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].day === day) {
        return i;
      }
    }
    return -1;
  }
  private updateData() {
    this.graphData = [];
    const plus = {name: 'Доход', series: []};
    const minus = {name: 'Расход', series: []};

    for (let i = 1; i <= 31; i++) {
      let index = this.findDay(this._donates, i);
      let value = 0;
      if (index !== -1) {
        value = this._donates[index].sum;
      }
      plus.series.push({
        name: i,
        value: value
      });

      index = this.findDay(this._payouts, i);
      value = 0;
      if (index !== -1) {
        value = this._payouts[index].sum;
      }
      minus.series.push({
        name: i,
        value: value
      });
    }

    this.graphData.push(plus);
    this.graphData.push(minus);
  }

  @Input() set donates(value: Array<any>) {
    this._donates = value;
    this.updateData();
  }
  @Input() set payouts(value: Array<any>) {
    this._payouts = value;
    this.updateData();
  }
}
