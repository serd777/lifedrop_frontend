import { Component, OnInit } from '@angular/core';
import { AdminStatisticService } from '../../../services/admin/statistic/admin-statistic.service';

@Component({
  selector: 'app-admin-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.css']
})
export class AdminIndexPageComponent implements OnInit {

  constructor(
      public statService: AdminStatisticService
  ) { }

  ngOnInit() {
  }

}
