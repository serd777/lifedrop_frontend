import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Item} from '../../../../models/item';
import {AdminItemsService} from '../../../../services/admin/items/admin-items.service';

@Component({
  selector: 'app-admin-item-info',
  templateUrl: './item-info.component.html',
  styleUrls: ['./item-info.component.css']
})
export class ItemInfoComponent implements OnInit {
  @Input() item: Item;
  @Output() onReturnBack = new EventEmitter<boolean>();

  constructor(
    private itemsService: AdminItemsService
  ) { }

  ngOnInit() {}
  returnBack() {
    this.onReturnBack.emit(true);
  }
  saveItem() {
    this.itemsService.updateItem(this.item);
    this.returnBack();
  }

  parseImg(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  private _handleReaderLoaded(evt) {
    const binaryString = evt.target.result;
    this.item.img = btoa(binaryString);
  }
}
