import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AdminItemsService} from '../../../../services/admin/items/admin-items.service';
import {Item} from '../../../../models/item';
import {BackendHost} from '../../../../routing/pathes';

@Component({
  selector: 'app-admin-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent implements OnInit {
  @Output() onItemSelected = new EventEmitter<Item>();

  public searchName: string;
  public page: number;

  constructor(
    public itemsService: AdminItemsService
  ) {
    this.page = 1;
    this.searchName = '';
  }

  ngOnInit() {}
  getImgUrl(item: Item) {
    return BackendHost + item.img_path;
  }
  selectItem(item: Item) {
    this.onItemSelected.emit(item);
  }
}
