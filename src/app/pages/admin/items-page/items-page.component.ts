import { Component, OnInit } from '@angular/core';
import {Item} from '../../../models/item';

@Component({
  selector: 'app-admin-items-page',
  templateUrl: './items-page.component.html',
  styleUrls: ['./items-page.component.css']
})
export class AdminItemsPageComponent implements OnInit {
  public selectedItem: Item;

  public isListShowed: boolean;
  public isItemSelected: boolean;

  constructor() {
    this.isItemSelected = false;
    this.isListShowed = true;
  }
  ngOnInit() {}

  showList() {
    this.isListShowed = true;
    this.isItemSelected = false;
  }
  selectItem(item: Item) {
    this.selectedItem = item;

    this.isListShowed = false;
    this.isItemSelected = true;
  }
}
