import { Component, OnInit } from '@angular/core';
import {Item} from '../../../../models/item';
import {AdminItemsService} from '../../../../services/admin/items/admin-items.service';

@Component({
  selector: 'app-admin-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {
  public newItem: Item;

  constructor(
    private itemsService: AdminItemsService
  ) {
    this.newItem = new Item();
  }

  ngOnInit() {}
  parseImg(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  addItem() {
    this.itemsService.addItem(this.newItem);

    this.newItem.name = '';
    this.newItem.price = 0;
    this.newItem.img = '';
  }

  private _handleReaderLoaded(evt) {
    const binaryString = evt.target.result;
    this.newItem.img = btoa(binaryString);
  }
}
