import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-admin-address-dialog',
  templateUrl: './address-dialog.html',
})
export class AdminAddressDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<AdminAddressDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
