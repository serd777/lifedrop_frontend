import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectingPayoutsComponent } from './collecting-payouts.component';

describe('CollectingPayoutsComponent', () => {
  let component: CollectingPayoutsComponent;
  let fixture: ComponentFixture<CollectingPayoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectingPayoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectingPayoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
