import { Component, OnInit } from '@angular/core';
import {AdminPayoutsService} from '../../../../services/admin/payouts/admin-payouts.service';
import {Drop} from '../../../../models/drop';
import {MatDialog} from '@angular/material';
import {AdminTrackCodeDialogComponent} from '../track-code-dialog/track-code-dialog';
import {AdminCommentDialogComponent} from '../comment-dialog/comment-dialog';
import {User} from '../../../../models/user';
import {AdminUsersService} from '../../../../services/admin/users/admin-users.service';
import {UserAddress} from '../../../../models/user_address';
import {AdminAddressDialogComponent} from '../address-dialog/address-dialog';

@Component({
  selector: 'app-admin-collecting-payouts',
  templateUrl: './collecting-payouts.component.html',
  styleUrls: ['./collecting-payouts.component.css']
})
export class AdminCollectingPayoutsComponent implements OnInit {
  public page: number;

  constructor(
    public payoutsService: AdminPayoutsService,
    private usersService: AdminUsersService,
    public dialogTrackCode: MatDialog,
    public dialogComment: MatDialog,
    public dialogAddress: MatDialog
  ) {
    this.page = 1;
  }

  ngOnInit() {}
  sendDrop(drop: Drop) {
    const dialogRef = this.dialogTrackCode.open(
      AdminTrackCodeDialogComponent,
      {
        width: '250px',
        data: {track_code: ''}
      });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        drop.track_code = result;
        this.payoutsService.markAsSent(drop);
      }
    });
  }
  commentClicked(drop: Drop) {
    this.payoutsService.getComment(drop)
      .then((comment: string) => {
        const dialogRef = this.dialogComment.open(
          AdminCommentDialogComponent,
          {
            width: '250px',
            data: {comment: comment}
          });
        dialogRef.afterClosed().subscribe(result => {
          if (result !== undefined) {
            this.payoutsService.setComment(drop, result);
          }
        });
      })
      .catch(() => {});
  }
  showAddress(user: User) {
    this.usersService.getUserAddress(user)
      .then((address: UserAddress) => {
        const dialogRef = this.dialogAddress.open(
          AdminAddressDialogComponent,
          {
            width: '400px',
            data: {address: address}
          });
      })
      .catch(() => {});
  }
}
