import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-admin-comment-dialog',
  templateUrl: './comment-dialog.html',
})
export class AdminCommentDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<AdminCommentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
