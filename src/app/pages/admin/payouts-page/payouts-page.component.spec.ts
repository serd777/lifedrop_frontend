import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutsPageComponent } from './payouts-page.component';

describe('PayoutsPageComponent', () => {
  let component: PayoutsPageComponent;
  let fixture: ComponentFixture<PayoutsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
