import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentPayoutsComponent } from './sent-payouts.component';

describe('SentPayoutsComponent', () => {
  let component: SentPayoutsComponent;
  let fixture: ComponentFixture<SentPayoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentPayoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentPayoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
