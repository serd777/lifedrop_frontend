import { Component, OnInit } from '@angular/core';
import {AdminPayoutsService} from '../../../../services/admin/payouts/admin-payouts.service';
import {Drop} from '../../../../models/drop';
import {AdminCommentDialogComponent} from '../comment-dialog/comment-dialog';
import {MatDialog} from '@angular/material';
import {AdminUsersService} from '../../../../services/admin/users/admin-users.service';
import {User} from '../../../../models/user';
import {AdminAddressDialogComponent} from '../address-dialog/address-dialog';
import {UserAddress} from '../../../../models/user_address';

@Component({
  selector: 'app-admin-sent-payouts',
  templateUrl: './sent-payouts.component.html',
  styleUrls: ['./sent-payouts.component.css']
})
export class AdminSentPayoutsComponent implements OnInit {
  public page: number;

  constructor(
    public payoutsService: AdminPayoutsService,
    private usersService: AdminUsersService,
    public dialogComment: MatDialog,
    public dialogAddress: MatDialog
  ) {
    this.page = 1;
  }

  ngOnInit() {}
  commentClicked(drop: Drop) {
    this.payoutsService.getComment(drop)
      .then((comment: string) => {
        const dialogRef = this.dialogComment.open(
          AdminCommentDialogComponent,
          {
            width: '250px',
            data: {comment: comment}
          });
        dialogRef.afterClosed().subscribe(result => {
          if (result !== undefined) {
            this.payoutsService.setComment(drop, result);
          }
        });
      })
      .catch(() => {});
  }
  showAddress(user: User) {
    this.usersService.getUserAddress(user)
      .then((address: UserAddress) => {
        const dialogRef = this.dialogAddress.open(
          AdminAddressDialogComponent,
          {
            width: '400px',
            data: {address: address}
          });
      })
      .catch(() => {});
  }
}
