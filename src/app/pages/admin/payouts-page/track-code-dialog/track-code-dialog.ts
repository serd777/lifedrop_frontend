import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-admin-track-code-dialog',
  templateUrl: './track-code-dialog.html',
})
export class AdminTrackCodeDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<AdminTrackCodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
