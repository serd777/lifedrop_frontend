import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingPayoutsComponent } from './waiting-payouts.component';

describe('WaitingPayoutsComponent', () => {
  let component: WaitingPayoutsComponent;
  let fixture: ComponentFixture<WaitingPayoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitingPayoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingPayoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
