import { Component, OnInit } from '@angular/core';
import { AdminPayoutsService } from '../../../../services/admin/payouts/admin-payouts.service';
import {Drop} from '../../../../models/drop';
import {MatDialog} from '@angular/material';
import {AdminCommentDialogComponent} from '../comment-dialog/comment-dialog';
import {User} from '../../../../models/user';
import {AdminAddressDialogComponent} from '../address-dialog/address-dialog';
import {UserAddress} from '../../../../models/user_address';
import {AdminUsersService} from '../../../../services/admin/users/admin-users.service';

@Component({
  selector: 'app-admin-waiting-payouts',
  templateUrl: './waiting-payouts.component.html',
  styleUrls: ['./waiting-payouts.component.css']
})
export class AdminWaitingPayoutsComponent implements OnInit {
  public page: number;

  constructor(
    public payoutsService: AdminPayoutsService,
    private usersService: AdminUsersService,
    public dialogComment: MatDialog,
    public dialogAddress: MatDialog
  ) {
    this.page = 1;
  }

  ngOnInit() {}
  commentClicked(drop: Drop) {
    this.payoutsService.getComment(drop)
      .then((comment: string) => {
        const dialogRef = this.dialogComment.open(
          AdminCommentDialogComponent,
          {
            width: '250px',
            data: {comment: comment}
          });
        dialogRef.afterClosed().subscribe(result => {
          if (result !== undefined) {
            this.payoutsService.setComment(drop, result);
          }
        });
      })
      .catch(() => {});
  }

  showAddress(user: User) {
    this.usersService.getUserAddress(user)
      .then((address: UserAddress) => {
        const dialogRef = this.dialogAddress.open(
          AdminAddressDialogComponent,
          {
            width: '400px',
            data: {address: address}
          });
      })
      .catch(() => {});
  }
}
