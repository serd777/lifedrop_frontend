import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPromoCodeComponent } from './new-promo-code.component';

describe('NewPromoCodeComponent', () => {
  let component: NewPromoCodeComponent;
  let fixture: ComponentFixture<NewPromoCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPromoCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPromoCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
