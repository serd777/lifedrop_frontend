import { Component, OnInit } from '@angular/core';
import {PromoCode} from '../../../../models/promo_code';
import {AdminPromoCodesService} from '../../../../services/admin/promo_codes/admin-promo-codes.service';

@Component({
  selector: 'app-admin-new-promo-code',
  templateUrl: './new-promo-code.component.html',
  styleUrls: ['./new-promo-code.component.css']
})
export class NewPromoCodeComponent implements OnInit {
  public newCode: PromoCode;

  constructor(
    private promoService: AdminPromoCodesService
  ) {
    this.newCode = new PromoCode();
  }

  ngOnInit() {}
  addPromo() {
    this.promoService.addCode(this.newCode);

    this.newCode.code = '';
    this.newCode.amount = 0;
    this.newCode.price = 0;
  }

}
