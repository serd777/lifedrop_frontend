import { Component, OnInit } from '@angular/core';
import {AdminPromoCodesService} from '../../../../services/admin/promo_codes/admin-promo-codes.service';

@Component({
  selector: 'app-admin-promo-codes-list',
  templateUrl: './promo-codes-list.component.html',
  styleUrls: ['./promo-codes-list.component.css']
})
export class PromoCodesListComponent implements OnInit {
  public page: number;

  constructor(
    public promoService: AdminPromoCodesService
  ) {
    this.page = 1;
  }

  ngOnInit() {
  }

}
