import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPromoOfferComponent } from './new-promo-offer.component';

describe('NewPromoOfferComponent', () => {
  let component: NewPromoOfferComponent;
  let fixture: ComponentFixture<NewPromoOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPromoOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPromoOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
