import { Component, OnInit } from '@angular/core';
import {AdminPromoOffersService} from '../../../../services/admin/promo_offers/admin-promo-offers.service';
import {PromoOffer} from '../../../../models/promo_offer';

@Component({
  selector: 'app-admin-new-promo-offer',
  templateUrl: './new-promo-offer.component.html',
  styleUrls: ['./new-promo-offer.component.css']
})
export class NewPromoOfferComponent implements OnInit {
  public newOffer: PromoOffer;

  constructor(
    private promoService: AdminPromoOffersService
  ) {
    this.newOffer = new PromoOffer();
  }

  ngOnInit() {}
  parseImg(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  addOffer() {
    this.promoService.addOffer(this.newOffer);

    this.newOffer.title = '';
    this.newOffer.about = '';
    this.newOffer.img = '';
  }

  private _handleReaderLoaded(evt) {
    const binaryString = evt.target.result;
    this.newOffer.img = btoa(binaryString);
  }
}

