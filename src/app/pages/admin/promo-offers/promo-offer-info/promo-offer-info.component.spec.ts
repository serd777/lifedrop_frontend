import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoOfferInfoComponent } from './promo-offer-info.component';

describe('PromoOfferInfoComponent', () => {
  let component: PromoOfferInfoComponent;
  let fixture: ComponentFixture<PromoOfferInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoOfferInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoOfferInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
