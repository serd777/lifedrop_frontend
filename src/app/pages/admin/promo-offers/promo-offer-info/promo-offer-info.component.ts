import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PromoOffer} from '../../../../models/promo_offer';
import {AdminPromoOffersService} from '../../../../services/admin/promo_offers/admin-promo-offers.service';

@Component({
  selector: 'app-admin-promo-offer-info',
  templateUrl: './promo-offer-info.component.html',
  styleUrls: ['./promo-offer-info.component.css']
})
export class AdminPromoOfferInfoComponent implements OnInit {
  @Input() offer: PromoOffer;
  @Output() onReturnBack = new EventEmitter<boolean>();

  constructor(
    private offersService: AdminPromoOffersService
  ) { }

  ngOnInit() {}
  returnBack() {
    this.onReturnBack.emit(true);
  }
  updateOffer() {
    this.offersService.updateOffer(this.offer);
    this.returnBack();
  }

  parseImg(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  private _handleReaderLoaded(evt) {
    const binaryString = evt.target.result;
    this.offer.img = btoa(binaryString);
  }
}
