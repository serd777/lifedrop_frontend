import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoOffersListComponent } from './promo-offers-list.component';

describe('PromoOffersListComponent', () => {
  let component: PromoOffersListComponent;
  let fixture: ComponentFixture<PromoOffersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoOffersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoOffersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
