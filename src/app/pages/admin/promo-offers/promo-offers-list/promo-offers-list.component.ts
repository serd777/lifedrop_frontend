import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AdminPromoOffersService} from '../../../../services/admin/promo_offers/admin-promo-offers.service';
import {PromoOffer} from '../../../../models/promo_offer';
import {BackendHost} from '../../../../routing/pathes';

@Component({
  selector: 'app-admin-promo-offers-list',
  templateUrl: './promo-offers-list.component.html',
  styleUrls: ['./promo-offers-list.component.css']
})
export class PromoOffersListComponent implements OnInit {
  @Output() onOfferSelected = new EventEmitter<PromoOffer>();
  public page: number;

  constructor(
    public promoService: AdminPromoOffersService
  ) {
    this.page = 1;
  }

  ngOnInit() {}
  getImgUrl(offer: PromoOffer) {
    return BackendHost + offer.img_path;
  }
  selectOffer(offer: PromoOffer) {
    this.onOfferSelected.emit(offer);
  }
}
