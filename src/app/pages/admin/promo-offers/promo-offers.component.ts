import { Component, OnInit } from '@angular/core';
import {PromoOffer} from '../../../models/promo_offer';

@Component({
  selector: 'app-admin-promo-offers',
  templateUrl: './promo-offers.component.html',
  styleUrls: ['./promo-offers.component.css']
})
export class AdminPromoOffersComponent implements OnInit {
  private _listShowed: boolean;
  public selectedOffer: PromoOffer;

  constructor() {
    this._listShowed = true;
  }

  ngOnInit() {}
  selectOffer(offer: PromoOffer) {
    this.selectedOffer = offer;
    this._listShowed = false;
  }

  get isListShowed() { return this._listShowed; }
  set listShowed(value: boolean) { this._listShowed = value; }
}
