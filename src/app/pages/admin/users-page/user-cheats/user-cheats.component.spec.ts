import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCheatsComponent } from './user-cheats.component';

describe('UserCheatsComponent', () => {
  let component: UserCheatsComponent;
  let fixture: ComponentFixture<UserCheatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCheatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCheatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
