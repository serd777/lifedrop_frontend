import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../../models/user';
import {Cheat} from '../../../../models/cheat';
import {AdminUsersService} from '../../../../services/admin/users/admin-users.service';
import {AdminItemsService} from '../../../../services/admin/items/admin-items.service';
import {Item} from '../../../../models/item';

@Component({
  selector: 'app-admin-user-cheats',
  templateUrl: './user-cheats.component.html',
  styleUrls: ['./user-cheats.component.css']
})
export class AdminUserCheatsComponent implements OnInit {
  @Output() onReturnBack = new EventEmitter<boolean>();

  private _user: User;
  private _cheats: Array<Cheat>;

  public selectedItem: Item;
  public amount: number;

  constructor(
    public usersService: AdminUsersService,
    public itemsService: AdminItemsService
  ) { }

  ngOnInit() {}
  addCheat() {
    this.usersService.addCheat(this._user, this.selectedItem, this.amount)
      .then((cheat: Cheat) => {
        this._cheats.push(cheat);
      })
      .catch(() => {});
  }

  @Input() set user(value: User) {
    this._user = value;
    this.usersService.getCheats(value)
      .then((cheats: Array<Cheat>) => {
        this._cheats = cheats;
      })
      .catch(() => {});
  }
  get user() { return this._user; }
  get cheats() { return this._cheats; }
}
