import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBoxesHistoryComponent } from './user-boxes-history.component';

describe('UserBoxesHistoryComponent', () => {
  let component: UserBoxesHistoryComponent;
  let fixture: ComponentFixture<UserBoxesHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBoxesHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBoxesHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
