import {Component, Input, OnInit} from '@angular/core';
import {Drop} from '../../../../../models/drop';

@Component({
  selector: 'app-admin-user-boxes-history',
  templateUrl: './user-boxes-history.component.html',
  styleUrls: ['./user-boxes-history.component.css']
})
export class AdminUserBoxesHistoryComponent implements OnInit {
  @Input() drops: Array<Drop>;
  public page: number;

  constructor() {
    this.drops = [];
    this.page = 1;
  }

  ngOnInit() {}
}
