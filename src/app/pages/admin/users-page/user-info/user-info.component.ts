import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../../models/user';
import {AdminUsersService} from '../../../../services/admin/users/admin-users.service';
import {AllUserStat} from '../../../../models/history/all_stat';
import {PaymentStat} from '../../../../models/history/payment_stat';
import {PromoStat} from '../../../../models/history/promo_stat';
import {ReferralStat} from '../../../../models/history/referral_stat';
import {OwnBoxesStat} from '../../../../models/history/own_boxes_stat';
import {Drop} from '../../../../models/drop';

@Component({
  selector: 'app-admin-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
  private _user: User;
  private _paymentStat: Array<PaymentStat>;
  private _promoStat: Array<PromoStat>;
  private _referralStat: Array<ReferralStat>;
  private _ownBoxesStat: Array<OwnBoxesStat>;
  private _dropsStat: Array<Drop>;
  private _referralCount: number;

  @Output() onBackClicked = new EventEmitter<boolean>();

  constructor(
    private usersService: AdminUsersService
  ) {
    this._paymentStat = [];
    this._promoStat = [];
    this._referralStat = [];
    this._ownBoxesStat = [];
    this._dropsStat = [];
    this._referralCount = 0;
  }

  get user() { return this._user; }
  @Input() set user(value: User) {
    this._user = value;
    this.usersService.getUserStat(this._user)
      .then((stat: AllUserStat) => {
        this._paymentStat = stat.payment_stat;
        this._promoStat = stat.promo_stat;
        this._referralStat  = stat.referral_stat;
        this._ownBoxesStat = stat.own_box_stat;
        this._dropsStat = stat.drops_stat;
        this._referralCount = stat.referrals_count;
      })
      .catch(err => console.log(err));
  }

  get paymentStat() { return this._paymentStat; }
  get promoStat() { return this._promoStat; }
  get referralStat() { return this._referralStat; }
  get ownBoxStat() { return this._ownBoxesStat; }
  get dropsStat() { return this._dropsStat; }
  get referralCount() { return this._referralCount; }

  ngOnInit() {}
  returnBack() {
    this.onBackClicked.emit(true);
  }
  updateBalance() {
    this.usersService.changeBalance(this.user);
  }
  updateBlockState() {
    this.usersService.changeBlockState(this.user);
  }
  updatePermissions() {
    this.usersService.changeRights(this.user);
  }
}
