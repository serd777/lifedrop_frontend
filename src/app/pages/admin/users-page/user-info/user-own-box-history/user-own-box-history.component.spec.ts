import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOwnBoxHistoryComponent } from './user-own-box-history.component';

describe('UserOwnBoxHistoryComponent', () => {
  let component: UserOwnBoxHistoryComponent;
  let fixture: ComponentFixture<UserOwnBoxHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserOwnBoxHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOwnBoxHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
