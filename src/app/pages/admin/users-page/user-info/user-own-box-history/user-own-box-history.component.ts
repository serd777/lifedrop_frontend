import {Component, Input, OnInit} from '@angular/core';
import {OwnBoxesStat} from '../../../../../models/history/own_boxes_stat';

@Component({
  selector: 'app-admin-user-own-box-history',
  templateUrl: './user-own-box-history.component.html',
  styleUrls: ['./user-own-box-history.component.css']
})
export class AdminUserOwnBoxHistoryComponent implements OnInit {
  @Input() ownBoxes: Array<OwnBoxesStat>;
  public page: number;

  constructor() {
    this.ownBoxes = [];
    this.page = 1;
  }

  ngOnInit() {}
}
