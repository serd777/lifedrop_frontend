import {Component, Input, OnInit} from '@angular/core';
import {PaymentStat} from '../../../../../models/history/payment_stat';

@Component({
  selector: 'app-admin-user-payment-history',
  templateUrl: './user-payment-history.component.html',
  styleUrls: ['./user-payment-history.component.css']
})
export class AdminUserPaymentHistoryComponent implements OnInit {
  @Input() payments: Array<PaymentStat>;
  public page: number;

  constructor() {
    this.payments = [];
    this.page = 1;
  }

  ngOnInit() {}

}
