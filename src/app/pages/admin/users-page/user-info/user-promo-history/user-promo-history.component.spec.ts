import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPromoHistoryComponent } from './user-promo-history.component';

describe('UserPromoHistoryComponent', () => {
  let component: UserPromoHistoryComponent;
  let fixture: ComponentFixture<UserPromoHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPromoHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPromoHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
