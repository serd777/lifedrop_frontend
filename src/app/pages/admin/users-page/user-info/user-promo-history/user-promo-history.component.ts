import {Component, Input, OnInit} from '@angular/core';
import {PromoStat} from '../../../../../models/history/promo_stat';

@Component({
  selector: 'app-admin-user-promo-history',
  templateUrl: './user-promo-history.component.html',
  styleUrls: ['./user-promo-history.component.css']
})
export class AdminUserPromoHistoryComponent implements OnInit {
  @Input() promos: Array<PromoStat>;
  public page: number;

  constructor() {
    this.promos = [];
    this.page = 1;
  }

  ngOnInit() {}
}
