import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserReferralHistoryComponent } from './user-referral-history.component';

describe('UserReferralHistoryComponent', () => {
  let component: UserReferralHistoryComponent;
  let fixture: ComponentFixture<UserReferralHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserReferralHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReferralHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
