import {Component, Input, OnInit} from '@angular/core';
import {ReferralStat} from '../../../../../models/history/referral_stat';

@Component({
  selector: 'app-admin-user-referral-history',
  templateUrl: './user-referral-history.component.html',
  styleUrls: ['./user-referral-history.component.css']
})
export class AdminUserReferralHistoryComponent implements OnInit {
  @Input() referrals: Array<ReferralStat>;
  public page: number;

  constructor() {
    this.referrals = [];
    this.page = 1;
  }

  ngOnInit() {}
}
