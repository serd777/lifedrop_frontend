import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AdminUsersService} from '../../../../services/admin/users/admin-users.service';
import {User} from '../../../../models/user';

@Component({
  selector: 'app-admin-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  //  Выходной сигнал
  @Output() onUserSelected = new EventEmitter<User>();
  //  Выходной сигнал для подкрутки
  @Output() onShowCheats = new EventEmitter<User>();
  // Текушая страница
  public page: number;
  // Имя поиска
  public searchName: string;

  constructor(
    public usersService: AdminUsersService
  ) {
    this.page = 1;
    this.searchName = '';
  }

  ngOnInit() {}
  selectUser(user: User) {
    this.onUserSelected.emit(user);
  }
  showCheats(user: User) {
    this.onShowCheats.emit(user);
  }
}

