import { Component, OnInit } from '@angular/core';
import {User} from '../../../models/user';

@Component({
  selector: 'app-admin-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.css']
})
export class AdminUsersPageComponent implements OnInit {
  public selectedUser: User;
  public selectedUserCheats: User;

  public isUserSelected: boolean;
  public isListShowed: boolean;
  public isCheatsShowed: boolean;

  constructor() {
    this.selectedUser = undefined;

    this.isCheatsShowed = false;
    this.isUserSelected = false;
    this.isListShowed = true;
  }
  ngOnInit() {}

  selectUser(user: User) {
    this.selectedUser = user;

    this.isUserSelected = true;
    this.isListShowed = false;
  }
  showCheats(user: User) {
    this.selectedUserCheats = user;

    this.isCheatsShowed = true;
    this.isListShowed = false;
  }
  showList() {
    this.isUserSelected = false;
    this.isCheatsShowed = false;
    this.isListShowed = true;
  }
}
