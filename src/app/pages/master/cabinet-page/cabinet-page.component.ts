import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {ModalNames, ModalsService} from '../../../services/modals/modals.service';
import {BackendHost, BackendRoutes} from '../../../routing/pathes';
import {Item} from '../../../models/item';
import {Drop} from '../../../models/drop';
import {GameService} from '../../../services/game/game.service';

@Component({
  selector: 'app-master-cabinet-page',
  templateUrl: './cabinet-page.component.html',
  styleUrls: ['./cabinet-page.component.css']
})
export class MasterCabinetPageComponent implements OnInit {
  public dropsPage: number;
  public payoutsPage: number;

  constructor(
    public userService: UserService,
    private gameService: GameService,
    private modalsService: ModalsService
  ) {
    this.dropsPage = 1;
    this.payoutsPage = 1;
  }

  ngOnInit() {}

  getLogOutPath() {
    return BackendRoutes.Logout;
  }
  getItemImg(item: Item) {
    return BackendHost + item.img_path;
  }
  showPromoCodeModal() {
    this.modalsService.showModal(ModalNames.PromoCodeModal);
  }
  showAddressModal() {
    this.modalsService.showModal(ModalNames.AddressModal);
  }

  possibleToClick(drop: Drop) {
    return drop.status === 'Unchecked' || drop.status === 'Sent';
  }
  dropClicked(drop: Drop) {
    if (drop.status === 'Unchecked') {
      this.gameService.prize = drop;
      this.modalsService.showModal(this.modalsService.getPrizeModalName());
    } else if (drop.status === 'Sent') {
      this.gameService.prize = drop;
      this.modalsService.showModal(this.modalsService.getTrackCodeModalName());
    }
  }
}
