import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-master-create-box-dialog',
  templateUrl: './create-box-dialog.html',
  styleUrls: ['./create-box-dialog.css']
})
export class MasterCreateBoxDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<MasterCreateBoxDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
