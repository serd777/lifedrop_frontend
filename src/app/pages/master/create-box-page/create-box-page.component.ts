import { Component, OnInit } from '@angular/core';
import {OwnBoxesService} from '../../../services/own_boxes/own-boxes.service';
import {OwnBoxImg} from '../../../models/own_box_img';
import {BackendHost} from '../../../routing/pathes';
import {Item} from '../../../models/item';
import {MatDialog} from '@angular/material';
import {MasterCreateBoxDialogComponent} from './create-box-dialog/create-box-dialog';
import {OwnBoxAvailItem} from '../../../models/own_box_avail_item';
import {UserService} from '../../../services/user/user.service';
import {OwnBox} from '../../../models/own_box';
import {Router} from '@angular/router';
import {fadeInAnimation} from '../../../animations/fadeIn';

@Component({
  selector: 'app-master-create-box-page',
  templateUrl: './create-box-page.component.html',
  styleUrls: ['./create-box-page.component.css'],
  animations: [fadeInAnimation]
})
export class MasterCreateBoxPageComponent implements OnInit {
  public imgPage: number;
  public itemsPage: number;

  public activeImg: OwnBoxImg;
  private selectedItems: Array<OwnBoxAvailItem>;
  public boxName: string;
  public price: number;

  constructor(
    public boxesService: OwnBoxesService,
    private userService: UserService,
    public dialogComment: MatDialog,
    private router: Router
  ) {
    this.imgPage = 1;
    this.itemsPage = 1;
    this.price = 0;
    this.boxName = '';
    this.activeImg = undefined;
    this.selectedItems = [];
  }

  ngOnInit() {}
  getBoxImg(img: OwnBoxImg) {
    return BackendHost + img.img_path;
  }
  getItemImg(item: Item) {
    return BackendHost + item.img_path;
  }

  itemClicked(item: OwnBoxAvailItem) {
    if (this.isSelectedItem(item)) {
      const index = this.selectedItems.indexOf(item);
      this.selectedItems.splice(index, 1);
    } else {
      this.selectedItems.push(item);
    }

    this.price = this.calcPrice();
  }
  isSelectedItem(item: OwnBoxAvailItem) {
    return this.selectedItems.indexOf(item) !== -1;
  }
  calcPrice() {
    if (!this.selectedItems.length) {
      return 0;
    }

    let average = 0;
    for (const item of this.selectedItems) {
      average += item.item.price;
    }
    average = Math.round(average / this.selectedItems.length);

    const minItem = this.findMinItem();
    let price = Math.round(average / 10);
    if (price < minItem.item.price) {
      price = minItem.item.price + Math.round(minItem.item.price * 0.2);
    }

    return price;
  }

  findMinItem() {
    let currentItem = this.selectedItems[0];
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (this.selectedItems[i].item.price < currentItem.item.price) {
        currentItem = this.selectedItems[i];
      }
    }
    return currentItem;
  }

  createBox() {
    if (this.activeImg === undefined) {
      this.showDialog('Вы не выбрали изображение кейса!');
      return;
    }
    if (this.boxName === '') {
      this.showDialog('Вы не ввели название кейса!');
      return;
    }
    if (!this.selectedItems.length) {
      this.showDialog('Вы не выбрали ни одного предмета!');
      return;
    }
    if (this.selectedItems.length < 5) {
      this.showDialog('Нужно выбрать как минимум 5 предметов!');
      return;
    }
    if (this.userService.userDrops.length < 5) {
      this.showDialog('Для создания кейса Вам необходимо открыть 5 обычных кейсов!');
      return;
    }

    this.boxesService.createBox(this.boxName, this.selectedItems, this.activeImg)
      .then((box: OwnBox) => {
        this.router.navigateByUrl('/');
      })
      .catch((err: string) => {
        this.showDialog(err);
      });
  }

  private showDialog(message: string) {
    const dialogRef = this.dialogComment.open(
      MasterCreateBoxDialogComponent,
      {
        width: '250px',
        data: {msg: message}
      });
  }
}
