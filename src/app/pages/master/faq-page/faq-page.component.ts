import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq-page',
  templateUrl: './faq-page.component.html',
  styleUrls: ['./faq-page.component.css']
})
export class MasterFaqPageComponent implements OnInit {
  private items_amount: number;
  private faq_flags: Array<boolean>;

  constructor() {
    this.items_amount = 9;
    this.faq_flags = [];

    for (let i = 0; i < this.items_amount; i++) {
      this.faq_flags.push(false);
    }
  }

  itemClicked(id) {
    this.faq_flags[id] = !this.faq_flags[id];
  }

  get flags() { return this.faq_flags; }

  ngOnInit() {}
}
