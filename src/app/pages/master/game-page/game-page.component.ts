import {Component, OnDestroy, OnInit} from '@angular/core';
import {GameService} from '../../../services/game/game.service';
import {BoxItem} from '../../../models/box_item';
import {BackendHost, BackendRoutes} from '../../../routing/pathes';
import {UserService} from '../../../services/user/user.service';
import {ModalsService} from '../../../services/modals/modals.service';
import {Item} from '../../../models/item';
import {WebSocketService} from '../../../services/websocket/web-socket.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BoxesService} from '../../../services/boxes/boxes.service';
import {Box} from '../../../models/box';
import {OwnBoxesService} from '../../../services/own_boxes/own-boxes.service';
import {OwnBox} from '../../../models/own_box';
import {Drop} from '../../../models/drop';

@Component({
  selector: 'app-master-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.css']
})
export class MasterGamePageComponent implements OnInit, OnDestroy {
  private _startedRolling: boolean;
  private _isUserBoxGame: boolean;
  private _hoveredItem: number;
  private subLoad: any;

  constructor(
    public gameService: GameService,
    public userService: UserService,
    private webSocket: WebSocketService,
    private modalsService: ModalsService,
    private boxesService: BoxesService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this._startedRolling = false;
    this._isUserBoxGame = false;
    this._hoveredItem = -1;
  }

  initCommonBox(id: number) {
    const box = this.boxesService.allBoxes.filter((item: Box) => {
      return item.id === id;
    });

    if (!box.length) {
      this.router.navigateByUrl('/');
    } else {
      this.gameService.box = box[0];
      this._isUserBoxGame = false;
    }
  }
  ngOnInit() {
    this.subLoad = this.route.params.subscribe((params) => {
      const id = +params['id'];
      this.initCommonBox(id);
    });
  }
  ngOnDestroy() {
    this.subLoad.unsubscribe();
  }

  getImg(boxItem: BoxItem) {
    return BackendHost + boxItem.item.img_path;
  }
  getItemImg(item: Item) {
    return BackendHost + item.img_path;
  }

  hasEnoughMoney() {
    if (!this.userService.isAuth) {
      return true;
    }

    return this.userService.user.balance >= this.gameService.box.price;
  }
  readyToPlay() {
    return this.hasEnoughMoney() && !this.gameService.isGameStarted && this.userService.isAuth;
  }
  openDonateModal() {
    this.modalsService.showModal(this.modalsService.getDonateModalName());
  }
  getAuthUrl() {
    return BackendRoutes.AuthVk;
  }

  get isRolling() { return this._startedRolling; }
  get isUserBoxGame() { return this._isUserBoxGame; }

  openBox() {
    this._startedRolling = false;
    this.gameService.gameStarted = true;

    this.gameService.openBox(this.gameService.box)
      .then((data) => {
        this._startedRolling = true;
        this.userService.user.balance -= this.gameService.box.price;
        this.webSocket.sendRequest('box_opened', this.gameService.prize.id);

        setTimeout(() => {
          this.gameService.gameStarted = false;
          this.userService.addDrop(this.gameService.prize);
          this.modalsService.showModal(this.modalsService.getPrizeModalName());
        }, 13000);
      })
      .catch((data) => {
        console.error(data);
      });
  }

  openOwnBox() {
    this._startedRolling = false;
    this.gameService.gameStarted = true;

    this.gameService.openOwnBox(this.gameService.ownBox)
      .then((data) => {
        this._startedRolling = true;
        this.userService.user.balance -= this.gameService.ownBox.price;
        this.webSocket.sendRequest('box_opened', this.gameService.prize.id);

        setTimeout(() => {
          this.gameService.gameStarted = false;
          this.userService.addDrop(this.gameService.prize);
          this.modalsService.showModal(this.modalsService.getPrizeModalName());
        }, 13000);
      })
      .catch((data) => {
        console.error(data);
      });
  }

  set itemHovered(value: number) { this._hoveredItem = value; }
  get itemHovered() { return this._hoveredItem; }

  hoverItemColor(drop: Drop): string {
    if (this.itemHovered === drop.id) {
      return this.gameService.box.border_color;
    }
    return 'transparent';
  }
}
