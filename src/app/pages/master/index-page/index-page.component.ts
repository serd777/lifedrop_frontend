import { Component, OnInit } from '@angular/core';
import {BoxesService} from '../../../services/boxes/boxes.service';
import {Box} from '../../../models/box';
import {BackendHost} from '../../../routing/pathes';
import {GameService} from '../../../services/game/game.service';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import {OwnBoxesService} from '../../../services/own_boxes/own-boxes.service';
import {OwnBox} from '../../../models/own_box';

@Component({
  selector: 'app-master-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.css']
})
export class MasterIndexPageComponent implements OnInit {
  public ownBoxPage: number;
  public sliderHeight: number;

  constructor(
    public boxesService: BoxesService,
    public userService: UserService,
    public router: Router
  ) {
    this.ownBoxPage = 1;
    this.sliderHeight = 0;
  }

  ngOnInit() {
    this.calcSliderHeight();
  }
  getBoxImg(box: Box) {
    return BackendHost + box.img_path;
  }
  getOwnBoxImg(box: OwnBox) {
    return BackendHost + box.img.img_path;
  }
  selectBox(box: Box) {
    this.router.navigateByUrl('/box/' + box.id);
  }
  selectOwnBox(box: OwnBox) {
    this.router.navigateByUrl('/user_box/' + box.id);
  }
  calcSliderHeight() {
    this.sliderHeight = 260 * Math.round(this.boxesService.boxesList.length / 3) + 50;
  }
}
