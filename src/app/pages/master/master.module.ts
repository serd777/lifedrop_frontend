import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterIndexPageComponent } from './index-page/index-page.component';
import { MasterFaqPageComponent } from './faq-page/faq-page.component';
import { MasterLicensePageComponent } from './license-page/license-page.component';
import { MasterReferralPageComponent } from './referral-page/referral-page.component';
import { MasterCabinetPageComponent } from './cabinet-page/cabinet-page.component';
import {BoxesService} from '../../services/boxes/boxes.service';
import {MasterOffersPageComponent} from './offers-page/offers-page.component';
import {OffersService} from '../../services/offers/offers.service';
import { MasterGamePageComponent } from './game-page/game-page.component';
import {GameService} from '../../services/game/game.service';
import {LiveDropService} from '../../services/live_drop/live-drop.service';
import {StatisticService} from '../../services/statistic/statistic.service';
import {PromoService} from '../../services/promo/promo.service';
import {NgxPaginationModule} from 'ngx-pagination';
import { MasterProfilePageComponent } from './profile-page/profile-page.component';
import {MatButtonModule, MatDialogModule, MatInputModule, MatTabsModule} from '@angular/material';
import {OnlyPayoutPipe} from '../../pipes/master/only_payouts/only-payout.pipe';
import { MasterCreateBoxPageComponent } from './create-box-page/create-box-page.component';
import {FormsModule} from '@angular/forms';
import {MasterCreateBoxDialogComponent} from './create-box-page/create-box-dialog/create-box-dialog';
import {CdkTableModule} from '@angular/cdk/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    NgxPaginationModule,
    MatTabsModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    CdkTableModule,
    MatDialogModule
  ],
  providers: [
    BoxesService,
    OffersService,
    GameService,
    StatisticService,
    LiveDropService,
    PromoService
  ],
  exports: [
      MasterIndexPageComponent
  ],
  declarations: [
    MasterIndexPageComponent,
    MasterFaqPageComponent,
    MasterLicensePageComponent,
    MasterReferralPageComponent,
    MasterCabinetPageComponent,
    MasterOffersPageComponent,
    MasterGamePageComponent,
    MasterProfilePageComponent,
    OnlyPayoutPipe,
    MasterCreateBoxPageComponent,
    MasterCreateBoxDialogComponent
  ],
  entryComponents: [
    MasterCreateBoxDialogComponent
  ]
})
export class MasterModule { }
