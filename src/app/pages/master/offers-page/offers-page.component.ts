import { Component, OnInit } from '@angular/core';
import {OffersService} from '../../../services/offers/offers.service';
import {BackendHost} from '../../../routing/pathes';
import {PromoOffer} from '../../../models/promo_offer';

@Component({
  selector: 'app-master-offers-page',
  templateUrl: './offers-page.component.html',
  styleUrls: ['./offers-page.component.css']
})
export class MasterOffersPageComponent implements OnInit {

  constructor(
    public offersService: OffersService
  ) { }

  ngOnInit() {}
  getImg(offer: PromoOffer) {
    return BackendHost + offer.img_path;
  }
}
