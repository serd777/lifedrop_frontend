import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import {Drop} from '../../../models/drop';
import {BackendHost} from '../../../routing/pathes';
import {Item} from '../../../models/item';

@Component({
  selector: 'app-master-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class MasterProfilePageComponent implements OnInit, OnDestroy {
  private subLoad: any;

  private _hoveredItem: number;
  private _name: string;
  private _vk_id: string;
  private _avatar: string;
  private _games_played: number;
  private _profit: number;
  private _drops: Array<Drop>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {
    this._name = '';
    this._vk_id = '';
    this._avatar = '';
    this._games_played = 0;
    this._profit = 0;
    this._drops = [];
    this._hoveredItem = -1;
  }

  ngOnInit() {
    this.subLoad = this.route.params.subscribe((params) => {
      const id = +params['id'];
      this.userService.getUserProfile(id)
        .then((data: any) => {
          this._name         = data.name;
          this._vk_id        = data.vk_id;
          this._avatar       = data.avatar;
          this._games_played = data.games_played;
          this._profit       = data.profit;

          if (data.drops.length > 25) {
            this._drops = data.drops.slice(0, 24);
          } else {
            this._drops = data.drops;
          }
        })
        .catch(() => this.router.navigateByUrl('/'));
    });
  }
  ngOnDestroy() {
    this.subLoad.unsubscribe();
  }
  getItemImg(item: Item) {
    return BackendHost + item.img_path;
  }

  get name() { return this._name; }
  get vkId() { return this._vk_id; }
  get avatar() { return this._avatar; }
  get gamesPlayed() { return this._games_played; }
  get profit() { return this._profit; }
  get drops()  { return this._drops; }

  set itemHovered(value: number) { this._hoveredItem = value; }
  get itemHovered() { return this._hoveredItem; }

  hoverItemColor(drop: Drop): string {
    if (this.itemHovered === drop.id) {
      return drop.border_color;
    }
    return 'transparent';
  }
}
