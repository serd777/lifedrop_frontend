import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {FrontendHost} from '../../../routing/pathes';

@Component({
  selector: 'app-referral-page',
  templateUrl: './referral-page.component.html',
  styleUrls: ['./referral-page.component.css']
})
export class MasterReferralPageComponent implements OnInit {

  constructor(
    public userService: UserService
  ) { }

  ngOnInit() {}
  getRefLink() {
    return FrontendHost + '/ref/' + this.userService.user.id;
  }
}
