import { Pipe, PipeTransform } from '@angular/core';
import {Item} from '../../../models/item';

@Pipe({
  name: 'adminItemName'
})
export class AdminItemNamePipe implements PipeTransform {
  transform(users: Array<Item>, name: string): Array<Item> {
    if (!users) { return []; }
    if (!name)  { return users; }

    name = name.toLowerCase();
    return users.filter( (it: Item) => it.name.toLowerCase().includes(name));
  }
}
