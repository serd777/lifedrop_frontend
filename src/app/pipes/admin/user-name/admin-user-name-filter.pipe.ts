import { Pipe, PipeTransform } from '@angular/core';
import {User} from '../../../models/user';

@Pipe({
  name: 'adminUserNameFilter'
})
export class AdminUserNameFilterPipe implements PipeTransform {
  transform(users: Array<User>, name: string): Array<User> {
    if (!users) { return []; }
    if (!name)  { return users; }

    name = name.toLowerCase();
    return users.filter( (it: User) => it.name.toLowerCase().includes(name));
  }
}
