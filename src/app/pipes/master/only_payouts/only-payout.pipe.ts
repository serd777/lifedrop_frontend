import { Pipe, PipeTransform } from '@angular/core';
import {Drop} from '../../../models/drop';

@Pipe({
  name: 'onlyPayout'
})
export class OnlyPayoutPipe implements PipeTransform {

  transform(drops: Array<Drop>, args?: any): Array<Drop> {
    return drops.filter((drop: Drop) => {
      return (drop.status !== 'Unchecked') && (drop.status !== 'Sold');
    });
  }

}
