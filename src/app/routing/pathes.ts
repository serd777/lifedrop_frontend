//  Путь до бэкэнда
export const BackendHost = 'https://backend.lifedrop.net';
//  Путь до frontend'а
export const FrontendHost = 'https://lifedrop.net';

//  Роуты Backend'а
export const BackendRoutes = {
  GetCsrfToken: BackendHost + '/user/csrf_token',
  GetUserInfo: BackendHost + '/user/info',
  GetUserProfile: BackendHost + '/user/profile',
  ActivatePromo: BackendHost + '/user/activate_promo',
  UpdateAddress: BackendHost + '/user/update_address',

  AuthVk: BackendHost + '/auth/vk',
  Logout: BackendHost + '/auth/logout',

  GetBoxes: BackendHost + '/boxes/list',
  GetBoxItems: BackendHost + '/boxes/items',
  GetOffers: BackendHost + '/offers/list',
  GetTopUsers: BackendHost + '/top_users/list',

  OpenBox: BackendHost + '/game/open_box',
  SellItem: BackendHost + '/game/sell_item',
  TakeItem: BackendHost + '/game/take_item',

  GetItems: BackendHost + '/items/list',
  GetOwnBoxImgs: BackendHost + '/own_boxes/list_img',
  CreateOwnBox: BackendHost + '/own_boxes/create',
  GetOwnBoxes: BackendHost + '/own_boxes/list',
  GetOwnBoxesItems: BackendHost + '/own_boxes/list_items',
  OpenOwnBox: BackendHost + '/own_boxes/open',
  OwnBoxesStat: BackendHost + '/own_boxes/stat',

  PaymentGateway: FrontendHost + '/payment/gateway',
};

//  Роуты админки
export const BackendAdminRoutes = {
  GetUsersList: BackendHost + '/admin/users/list',
  GetUserDetails: BackendHost + '/admin/users/user_info',
  GetCheats: BackendHost + '/admin/users/get_cheats',
  AddCheat: BackendHost + '/admin/users/add_cheat',
  DeleteCheat: BackendHost + '/admin/users/del_cheat',
  ChangeBalance: BackendHost + '/admin/users/change_balance',
  ChangeBanState: BackendHost + '/admin/users/change_block',
  ChangeRights: BackendHost + '/admin/users/change_rights',
  GetUserAddress: BackendHost + '/admin/users/get_address',
  GetUserStat: BackendHost + '/admin/users/user_stat',

  GetSiteStatistic: BackendHost + '/admin/statistic/info',

  GetItemsList: BackendHost + '/admin/items/list',
  AddItem: BackendHost + '/admin/items/add',
  DeleteItem: BackendHost + '/admin/items/delete',
  UpdateItem: BackendHost + '/admin/items/update',
  UpdateItemImg: BackendHost + '/admin/items/update_img',

  GetBoxesSections: BackendHost + '/admin/boxes/sections/list',
  AddBoxSection: BackendHost + '/admin/boxes/sections/add',
  DeleteBoxSection: BackendHost + '/admin/boxes/sections/delete',
  UpdateBoxSection: BackendHost + '/admin/boxes/sections/update',

  GetBoxes: BackendHost + '/admin/boxes/list',
  AddBox: BackendHost + '/admin/boxes/add',
  DeleteBox: BackendHost + '/admin/boxes/delete',
  UpdateBox: BackendHost + '/admin/boxes/update',
  UpdateBoxImg: BackendHost + '/admin/boxes/update_img',

  GetBoxItems: BackendHost + '/admin/boxes/items/list',
  AddItemToBox: BackendHost + '/admin/boxes/items/add',
  DeleteItemFromBox: BackendHost + '/admin/boxes/items/delete',
  UpdateBoxItem: BackendHost + '/admin/boxes/items/update',

  GetPromoOffers: BackendHost + '/admin/promo_offers/list',
  AddPromoOffer: BackendHost + '/admin/promo_offers/add',
  DeletePromoOffer: BackendHost + '/admin/promo_offers/delete',
  UpdatePromoOffer: BackendHost + '/admin/promo_offers/update',
  UpdatePromoOfferImg: BackendHost + '/admin/promo_offers/update_img',

  GetPromoCodes: BackendHost + '/admin/promo_codes/list',
  AddPromoCode: BackendHost + '/admin/promo_codes/add',
  DeletePromoCode: BackendHost + '/admin/promo_codes/delete',

  GetWaitingPayouts: BackendHost + '/admin/payouts/list/waiting',
  GetCollectingPayouts: BackendHost + '/admin/payouts/list/collecting',
  GetSentPayouts: BackendHost + '/admin/payouts/list/sent',
  MarkPayoutCollecting: BackendHost + '/admin/payouts/mark_as_collecting',
  MarkPayoutSent: BackendHost + '/admin/payouts/mark_as_sent',
  GetPayoutComment: BackendHost + '/admin/payouts/get_comment',
  AddPayoutComment: BackendHost + '/admin/payouts/add_comment',

  GetOwnBoxImages: BackendHost + '/admin/own_boxes/list_img',
  AddOwnBoxImage: BackendHost + '/admin/own_boxes/add_img',
  DeleteOwnBoxImage: BackendHost + '/admin/own_boxes/delete_img',
  GetOwnBoxAvailItems: BackendHost + '/admin/own_boxes/list_available_items',
  AddOwnBoxAvailItem: BackendHost + '/admin/own_boxes/add_avail_item',
  DelOwnBoxAvailItem: BackendHost + '/admin/own_boxes/del_avail_item'
};
