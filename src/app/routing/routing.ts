import { MasterIndexPageComponent } from '../pages/master/index-page/index-page.component';
import { AdminIndexPageComponent } from '../pages/admin/index-page/index-page.component';
import { AdminUsersPageComponent } from '../pages/admin/users-page/users-page.component';
import { AdminItemsPageComponent } from '../pages/admin/items-page/items-page.component';
import { AdminCasesPageComponent } from '../pages/admin/cases-page/cases-page.component';
import { AdminPromoCodesComponent } from '../pages/admin/promo-codes/promo-codes.component';
import { AdminPayoutsPageComponent } from '../pages/admin/payouts-page/payouts-page.component';
import { AdminPromoOffersComponent } from '../pages/admin/promo-offers/promo-offers.component';
import {MasterFaqPageComponent} from '../pages/master/faq-page/faq-page.component';
import {MasterLicensePageComponent} from '../pages/master/license-page/license-page.component';
import {MasterReferralPageComponent} from '../pages/master/referral-page/referral-page.component';
import {IsAuthGuard} from '../guards/is-auth/is-auth.guard';
import {IsAdminGuard} from '../guards/is-admin/is-admin.guard';
import {MasterCabinetPageComponent} from '../pages/master/cabinet-page/cabinet-page.component';
import {MasterOffersPageComponent} from '../pages/master/offers-page/offers-page.component';
import {MasterGamePageComponent} from '../pages/master/game-page/game-page.component';
import {GameInitedGuard} from '../guards/game-inited/game-inited.guard';
import {MasterProfilePageComponent} from '../pages/master/profile-page/profile-page.component';
import {MasterCreateBoxPageComponent} from '../pages/master/create-box-page/create-box-page.component';

//  Роуты приложения
export const AppRoutes = [
  {path: '', component: MasterIndexPageComponent, canActivate: [GameInitedGuard]},
  {path: 'faq', component: MasterFaqPageComponent},
  {path: 'license', component: MasterLicensePageComponent},
  {path: 'offers', component: MasterOffersPageComponent},
  {path: 'referral', component: MasterReferralPageComponent, canActivate: [IsAuthGuard]},
  {path: 'cabinet', component: MasterCabinetPageComponent, canActivate: [IsAuthGuard]},
  {path: 'box/:id', component: MasterGamePageComponent, canActivate: [GameInitedGuard]},
  {path: 'profile/:id', component: MasterProfilePageComponent},

  {path: 'admin', component: AdminIndexPageComponent, canActivate: [IsAuthGuard, IsAdminGuard]},
  {path: 'admin/users', component: AdminUsersPageComponent, canActivate: [IsAuthGuard, IsAdminGuard]},
  {path: 'admin/items', component: AdminItemsPageComponent, canActivate: [IsAuthGuard, IsAdminGuard]},
  {path: 'admin/cases', component: AdminCasesPageComponent, canActivate: [IsAuthGuard, IsAdminGuard]},
  {path: 'admin/promo_codes', component: AdminPromoCodesComponent, canActivate: [IsAuthGuard, IsAdminGuard]},
  {path: 'admin/payouts', component: AdminPayoutsPageComponent, canActivate: [IsAuthGuard, IsAdminGuard]},
  {path: 'admin/promo_offers', component: AdminPromoOffersComponent, canActivate: [IsAuthGuard, IsAdminGuard]}
];
