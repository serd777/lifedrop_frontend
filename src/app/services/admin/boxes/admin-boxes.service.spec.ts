import { TestBed, inject } from '@angular/core/testing';

import { AdminBoxesService } from './admin-boxes.service';

describe('AdminBoxesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminBoxesService]
    });
  });

  it('should be created', inject([AdminBoxesService], (service: AdminBoxesService) => {
    expect(service).toBeTruthy();
  }));
});
