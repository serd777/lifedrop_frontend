import { Injectable } from '@angular/core';
import {Box} from '../../../models/box';
import {RequestsService, ServerAnswer} from '../../requests/requests.service';
import {BackendAdminRoutes} from '../../../routing/pathes';
import {BoxItem} from '../../../models/box_item';

@Injectable()
export class AdminBoxesService {
  private boxes: Array<Box>;

  constructor(
    private requests: RequestsService
  ) {
    this.initService();
  }

  private initService() {
    this.boxes = [];
    this.updateBoxes();
  }

  public updateBoxes() {
    this.requests.getRequest(
      BackendAdminRoutes.GetBoxes,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.boxes = answer.data;
        }
      });
  }
  public addBox(box: Box) {
    this.requests.postRequest(
      BackendAdminRoutes.AddBox,
      box,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.boxes.push(answer.data);
        }
      });
  }
  public updateBox(box: Box) {
    this.requests.postRequest(
      BackendAdminRoutes.UpdateBox,
      box,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.boxes.indexOf(box);
          this.boxes[index] = answer.data;

          if (box.img !== undefined) {
            this.boxes[index].img = box.img;
            this.updateBoxImg(this.boxes[index]);
          }
        }
      });
  }
  public updateBoxImg(box: Box) {
    this.requests.postRequest(
      BackendAdminRoutes.UpdateBoxImg,
      box,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.boxes.indexOf(box);
          this.boxes[index] = answer.data;
        }
      });
  }
  public deleteBox(box: Box) {
    this.requests.postRequest(
      BackendAdminRoutes.DeleteBox,
      box,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.boxes.indexOf(box);
          this.boxes.splice(index, 1);
        }
      });
  }
  public getBoxItems(box: Box) {
    return new Promise<Array<BoxItem>>((resolve, reject) => {
      this.requests.postRequest(
        BackendAdminRoutes.GetBoxItems,
        box,
        (answer: ServerAnswer) => {
          resolve(answer.data);
        });
    });
  }
  public addBoxItem(boxItem: BoxItem) {
    return new Promise<BoxItem>((resolve, reject) => {
      this.requests.postRequest(
        BackendAdminRoutes.AddItemToBox,
        boxItem,
        (answer: ServerAnswer) => {
          (answer.status) ? resolve(answer.data) : reject(answer.data);
        });
    });
  }
  public deleteBoxItem(boxItem: BoxItem) {
    this.requests.postRequest(BackendAdminRoutes.DeleteItemFromBox, boxItem);
  }
  public updateDropPercent(boxItem: BoxItem) {
    this.requests.postRequest(BackendAdminRoutes.UpdateBoxItem, boxItem);
  }

  get boxesList() { return this.boxes; }
}
