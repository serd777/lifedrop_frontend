import { TestBed, inject } from '@angular/core/testing';

import { AdminBoxesSectionsService } from './admin-boxes-sections.service';

describe('AdminBoxesSectionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminBoxesSectionsService]
    });
  });

  it('should be created', inject([AdminBoxesSectionsService], (service: AdminBoxesSectionsService) => {
    expect(service).toBeTruthy();
  }));
});
