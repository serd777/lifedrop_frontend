import { Injectable } from '@angular/core';
import {BoxSection} from '../../../models/box_section';
import {RequestsService, ServerAnswer} from '../../requests/requests.service';
import {BackendAdminRoutes} from '../../../routing/pathes';

@Injectable()
export class AdminBoxesSectionsService {
  private sections: Array<BoxSection>;

  constructor(
    private requests: RequestsService
  ) {
    this.initService();
  }

  private initService() {
    this.sections = [];
    this.updateSections();
  }

  public updateSections() {
    this.requests.getRequest(
      BackendAdminRoutes.GetBoxesSections,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.sections = answer.data;
        }
      });
  }

  public addSection(section: BoxSection) {
    this.requests.postRequest(
      BackendAdminRoutes.AddBoxSection,
      section,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.sections.push(answer.data);
        }
      });
  }
  public deleteSection(section: BoxSection) {
    this.requests.postRequest(
      BackendAdminRoutes.DeleteBoxSection,
      section,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.sections.indexOf(section);
          this.sections.splice(index, 1);
        }
      });
  }
  public updateSection(section: BoxSection) {
    this.requests.postRequest(
      BackendAdminRoutes.UpdateBoxSection,
      section,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.sections.indexOf(section);
          this.sections[index] = answer.data;
        }
      });
  }

  get sectionsList() { return this.sections; }
}
