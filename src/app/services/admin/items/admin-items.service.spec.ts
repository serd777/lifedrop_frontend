import { TestBed, inject } from '@angular/core/testing';

import { AdminItemsService } from './admin-items.service';

describe('AdminItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminItemsService]
    });
  });

  it('should be created', inject([AdminItemsService], (service: AdminItemsService) => {
    expect(service).toBeTruthy();
  }));
});
