import { Injectable } from '@angular/core';
import {RequestsService, ServerAnswer} from '../../requests/requests.service';
import {Item} from '../../../models/item';
import {BackendAdminRoutes} from '../../../routing/pathes';

//  Сервис взаимодействия с предметами
@Injectable()
export class AdminItemsService {
  private items: Array<Item>;

  constructor(
    private requests: RequestsService
  ) {
    this.initService();
  }
  private initService() {
    this.items = [];
    this.updateItems();
  }

  //  Метод, обновляющий список предметов
  public updateItems() {
    this.requests.getRequest(
      BackendAdminRoutes.GetItemsList,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.items = answer.data;
        }
      });
  }
  //  Метод, добавляющий новый предмет
  public addItem(item: Item) {
    this.requests.postRequest(
      BackendAdminRoutes.AddItem,
      item,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.items.push(answer.data);
        }
      });
  }
  //  Метод, удаляющий указанный предмет
  public deleteItem(item: Item) {
    this.requests.postRequest(
      BackendAdminRoutes.DeleteItem,
      item,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.items.indexOf(item);
          this.items.splice(index, 1);
        }
      });
  }
  //  Метод, обновляющий предмет
  public updateItem(item: Item) {
    this.requests.postRequest(
      BackendAdminRoutes.UpdateItem,
      item,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.items.indexOf(item);
          this.items[index] = answer.data;

          if (item.img !== undefined) {
            this.items[index].img = item.img;
            this.updateItemImg(this.items[index]);
          }
        }
      });
  }
  //  Метод обновляющий картинку предмета
  public updateItemImg(item: Item) {
    this.requests.postRequest(
      BackendAdminRoutes.UpdateItemImg,
      item,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.items.indexOf(item);
          this.items[index] = answer.data;
        }
      });
  }

  get itemsList() { return this.items; }
}
