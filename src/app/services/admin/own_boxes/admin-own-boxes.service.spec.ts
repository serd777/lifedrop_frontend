import { TestBed, inject } from '@angular/core/testing';

import { AdminOwnBoxesService } from './admin-own-boxes.service';

describe('AdminOwnBoxesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminOwnBoxesService]
    });
  });

  it('should be created', inject([AdminOwnBoxesService], (service: AdminOwnBoxesService) => {
    expect(service).toBeTruthy();
  }));
});
