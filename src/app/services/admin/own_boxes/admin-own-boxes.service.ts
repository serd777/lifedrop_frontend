import { Injectable } from '@angular/core';
import {OwnBoxImg} from '../../../models/own_box_img';
import {RequestsService, ServerAnswer} from '../../requests/requests.service';
import {BackendAdminRoutes} from '../../../routing/pathes';
import {OwnBoxAvailItem} from '../../../models/own_box_avail_item';
import {Item} from '../../../models/item';

@Injectable()
export class AdminOwnBoxesService {
  private _images: Array<OwnBoxImg>;
  private _avail_items: Array<OwnBoxAvailItem>;

  constructor(
    private requests: RequestsService
  ) {
    this._images = [];
    this._avail_items = [];

    this.updateImages();
    this.updateAvailItems();
  }

  private updateImages() {
    this.requests.getRequest(
      BackendAdminRoutes.GetOwnBoxImages,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._images = answer.data;
        }
      });
  }
  private updateAvailItems() {
    this.requests.getRequest(
      BackendAdminRoutes.GetOwnBoxAvailItems,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._avail_items = answer.data;
        }
      });
  }

  public addImage(img: string) {
    this.requests.postRequest(
      BackendAdminRoutes.AddOwnBoxImage,
      {img: img},
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._images.push(answer.data);
        }
      });
  }
  public deleteImage(img: OwnBoxImg) {
    this.requests.postRequest(
      BackendAdminRoutes.DeleteOwnBoxImage,
      img,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this._images.indexOf(img);
          this._images.splice(index, 1);
        }
      });
  }

  public addAvailItem(item: Item) {
    this.requests.postRequest(
      BackendAdminRoutes.AddOwnBoxAvailItem,
      item,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._avail_items.push(answer.data);
        }
      });
  }
  public delAvailItem(item: OwnBoxAvailItem) {
    this.requests.postRequest(
      BackendAdminRoutes.DelOwnBoxAvailItem,
      item,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this._avail_items.indexOf(item);
          this._avail_items.splice(index, 1);
        }
      });
  }

  get boxImages() { return this._images; }
  get availItems() { return this._avail_items; }
}
