import { Injectable } from '@angular/core';
import {Drop} from '../../../models/drop';
import {RequestsService, ServerAnswer} from '../../requests/requests.service';
import {BackendAdminRoutes} from '../../../routing/pathes';

@Injectable()
export class AdminPayoutsService {
  private _waiting_drops: Array<Drop>;
  private _collecting_drops: Array<Drop>;
  private _sent_drops: Array<Drop>;

  constructor(
    private requests: RequestsService
  ) {
    this._waiting_drops = [];
    this._collecting_drops = [];
    this._sent_drops = [];

    this.updateCollectingDrops();
    this.updateSentDrops();
    this.updateWaitingDrops();
  }

  private updateWaitingDrops() {
    this.requests.getRequest(
      BackendAdminRoutes.GetWaitingPayouts,
      (answer: ServerAnswer) => {
        this._waiting_drops = answer.data;
      });
  }
  private updateCollectingDrops() {
    this.requests.getRequest(
      BackendAdminRoutes.GetCollectingPayouts,
      (answer: ServerAnswer) => {
        this._collecting_drops = answer.data;
      });
  }
  private updateSentDrops() {
    this.requests.getRequest(
      BackendAdminRoutes.GetSentPayouts,
      (answer: ServerAnswer) => {
        this._sent_drops = answer.data;
      });
  }

  public markAsCollecting(drop: Drop) {
    this.requests.postRequest(
      BackendAdminRoutes.MarkPayoutCollecting,
      drop,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this._waiting_drops.indexOf(drop);
          this._waiting_drops.splice(index, 1);

          this._collecting_drops.push(drop);
        }
      });
  }
  public markAsSent(drop: Drop) {
    this.requests.postRequest(
      BackendAdminRoutes.MarkPayoutSent,
      drop,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this._collecting_drops.indexOf(drop);
          this._collecting_drops.splice(index, 1);

          this._sent_drops.push(drop);
        }
      });
  }

  public getComment(drop: Drop) {
    return new Promise<string>((resolve, reject) => {
      this.requests.postRequest(
        BackendAdminRoutes.GetPayoutComment,
        drop,
        (answer: ServerAnswer) => {
          if (answer.status) {
            resolve(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }
  public setComment(drop: Drop, comment: string) {
    this.requests.postRequest(
      BackendAdminRoutes.AddPayoutComment,
      {id: drop.id, comment: comment}
    );
  }

  get waitingDrops()    { return this._waiting_drops; }
  get collectingDrops() { return this._collecting_drops; }
  get sentDrops()       { return this._sent_drops; }
}
