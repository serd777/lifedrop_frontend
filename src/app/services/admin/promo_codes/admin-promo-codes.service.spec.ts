import { TestBed, inject } from '@angular/core/testing';

import { AdminPromoCodesService } from './admin-promo-codes.service';

describe('AdminPromoCodesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminPromoCodesService]
    });
  });

  it('should be created', inject([AdminPromoCodesService], (service: AdminPromoCodesService) => {
    expect(service).toBeTruthy();
  }));
});
