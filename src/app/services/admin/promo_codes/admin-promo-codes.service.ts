import { Injectable } from '@angular/core';
import {PromoCode} from '../../../models/promo_code';
import {RequestsService, ServerAnswer} from '../../requests/requests.service';
import {BackendAdminRoutes} from '../../../routing/pathes';

@Injectable()
export class AdminPromoCodesService {
  private codes: Array<PromoCode>;

  constructor(
    private requests: RequestsService
  ) {
    this.initService();
  }

  private initService() {
    this.codes = [];
    this.updateCodes();
  }

  public updateCodes() {
    this.requests.getRequest(
      BackendAdminRoutes.GetPromoCodes,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.codes = answer.data;
        }
      });
  }
  public addCode(code: PromoCode) {
    this.requests.postRequest(
      BackendAdminRoutes.AddPromoCode,
      code,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.codes.push(answer.data);
        }
      });
  }
  public deleteCode(code: PromoCode) {
    this.requests.postRequest(
      BackendAdminRoutes.DeletePromoCode,
      code,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.codes.indexOf(code);
          this.codes.splice(index, 1);
        }
      });
  }

  get codesList() { return this.codes; }
}
