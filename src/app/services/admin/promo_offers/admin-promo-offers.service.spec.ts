import { TestBed, inject } from '@angular/core/testing';

import { AdminPromoOffersService } from './admin-promo-offers.service';

describe('AdminPromoOffersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminPromoOffersService]
    });
  });

  it('should be created', inject([AdminPromoOffersService], (service: AdminPromoOffersService) => {
    expect(service).toBeTruthy();
  }));
});
