import { Injectable } from '@angular/core';
import {PromoOffer} from '../../../models/promo_offer';
import {RequestsService, ServerAnswer} from '../../requests/requests.service';
import {BackendAdminRoutes} from '../../../routing/pathes';

@Injectable()
export class AdminPromoOffersService {
  private offers: Array<PromoOffer>;

  constructor(
    private requests: RequestsService
  ) {
    this.initService();
  }

  private initService() {
    this.offers = [];
    this.updateOffers();
  }

  public updateOffers() {
    this.requests.getRequest(
      BackendAdminRoutes.GetPromoOffers,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.offers = answer.data;
        }
      });
  }

  public addOffer(offer: PromoOffer) {
    this.requests.postRequest(
      BackendAdminRoutes.AddPromoOffer,
      offer,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.offers.push(answer.data);
        }
      });
  }
  public deleteOffer(offer: PromoOffer) {
    this.requests.postRequest(
      BackendAdminRoutes.DeletePromoOffer,
      offer,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.offers.indexOf(offer);
          this.offers.splice(index, 1);
        }
      });
  }
  public updateOffer(offer: PromoOffer) {
    this.requests.postRequest(
      BackendAdminRoutes.UpdatePromoOffer,
      offer,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.offers.indexOf(offer);
          this.offers[index] = answer.data;

          if (offer.img !== undefined) {
            this.offers[index].img = offer.img;
            this.updateOfferImg(this.offers[index]);
          }
        }
      });
  }
  public updateOfferImg(offer: PromoOffer) {
    this.requests.postRequest(
      BackendAdminRoutes.UpdatePromoOfferImg,
      offer,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.offers.indexOf(offer);
          this.offers[index] = answer.data;
        }
      });
  }

  get offersList() { return this.offers; }
}
