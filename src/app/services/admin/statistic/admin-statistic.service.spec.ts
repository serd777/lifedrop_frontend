import { TestBed, inject } from '@angular/core/testing';

import { AdminStatisticService } from './admin-statistic.service';

describe('AdminStatisticService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminStatisticService]
    });
  });

  it('should be created', inject([AdminStatisticService], (service: AdminStatisticService) => {
    expect(service).toBeTruthy();
  }));
});
