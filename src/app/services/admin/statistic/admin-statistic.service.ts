import { Injectable } from '@angular/core';
import { RequestsService, ServerAnswer } from '../../requests/requests.service';
import { BackendAdminRoutes } from '../../../routing/pathes';

// Ответ от сервера
interface StatType {
  today_payments: number;
  week_payments: number;
  month_payments: number;

  today_payouts: number;
  week_payouts: number;
  month_payouts: number;

  total_users: number;
  games_played_today: number;

  donates_by_day: Array<any>;
  payouts_by_day: Array<any>;
}

@Injectable()
export class AdminStatisticService {
  // Статистика дохода
  private donates: {
    today: number,
    week:  number,
    month: number,
  };
  // Статистика расхода
  private payouts: {
    today: number,
    week:  number,
    month: number
  };
  // Статистика прибыли
  private profits: {
    today: number,
    week:  number,
    month: number
  };
  // Статистика сайта
  private siteStat: {
    registered:  number,
    casesOpened: number
  };
  // Доход по дням
  private _donatesByDay: Array<any>;
  // Расход по дням
  private _payoutsByDay: Array<any>;

  constructor(
    private requests: RequestsService
  ) {
    this.initService();
  }

  private initService() {
    this.donates = {today: 0, week: 0, month: 0};
    this.payouts = {today: 0, week: 0, month: 0};
    this.profits = {today: 0, week: 0, month: 0};
    this._donatesByDay = [];
    this._payoutsByDay = [];

    this.siteStat = {registered: 0, casesOpened: 0};
    this.updateStat();
  }

  public updateStat() {
    this.requests.getRequest(
      BackendAdminRoutes.GetSiteStatistic,
      (answer: ServerAnswer) => {
        const data: StatType = (answer.data);

        this.donates.today = data.today_payments;
        this.donates.week  = data.week_payments;
        this.donates.month = data.month_payments;

        this.payouts.today = data.today_payouts;
        this.payouts.week = data.week_payouts;
        this.payouts.month = data.month_payouts;

        this.profits.today = this.donates.today - this.payouts.today;
        this.profits.week = this.donates.week - this.payouts.week;
        this.profits.month = this.donates.month - this.payouts.month;

        this.siteStat.registered = data.total_users;
        this.siteStat.casesOpened = data.games_played_today;

        this._donatesByDay = data.donates_by_day;
        this._payoutsByDay = data.payouts_by_day;
      });
  }

  get donatesToday()  { return this.donates.today; }
  get donatesWeek()   { return this.donates.week;  }
  get donatesMonth()  { return this.donates.month; }

  get payoutsToday()  { return this.payouts.today; }
  get payoutsWeek()   { return this.payouts.week;  }
  get payoutsMonth()  { return this.payouts.month; }

  get profitToday()   { return this.profits.today; }
  get profitWeek()    { return this.profits.week;  }
  get profitMonth()   { return this.profits.month; }

  get registeredAmount() { return this.siteStat.registered; }
  get casesOpened()      { return this.siteStat.casesOpened; }

  get donatesByDay() { return this._donatesByDay; }
  get payoutsByDay() { return this._payoutsByDay; }
}

