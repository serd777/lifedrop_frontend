import { Injectable } from '@angular/core';
import {User} from '../../../models/user';
import {RequestsService, ServerAnswer} from '../../requests/requests.service';
import {BackendAdminRoutes} from '../../../routing/pathes';
import {Cheat} from '../../../models/cheat';
import {Item} from '../../../models/item';
import {UserAddress} from '../../../models/user_address';
import {AllUserStat} from '../../../models/history/all_stat';

@Injectable()
export class AdminUsersService {
  // Список пользователей
  private users: Array<User>;

  constructor(
    private requests: RequestsService
  ) {
    this.initService();
  }

  private initService() {
    this.users = [];
    this.updateUsers();
  }

  public updateUsers() {
    this.requests.getRequest(
      BackendAdminRoutes.GetUsersList,
      (answer: ServerAnswer) => {
        if (answer.status) {
          answer.data = this.sortByBalance(answer.data);
          answer.data = this.sortByRights(answer.data);

          this.users = answer.data;
        }
      });
  }
  public changeBalance(user: User) {
    this.requests.postRequest(
      BackendAdminRoutes.ChangeBalance,
      user,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.users.indexOf(user);
          this.users[index] = answer.data;
        }
      });
  }
  public changeBlockState(user: User) {
    this.requests.postRequest(
      BackendAdminRoutes.ChangeBanState,
      user,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.users.indexOf(user);
          this.users[index] = answer.data;
        }
      });
  }
  public changeRights(user: User) {
    this.requests.postRequest(
      BackendAdminRoutes.ChangeRights,
      user,
      (answer: ServerAnswer) => {
        if (answer.status) {
          const index = this.users.indexOf(user);
          this.users[index] = answer.data;
        }
      });
  }

  public getCheats(user: User) {
    return new Promise<Array<Cheat>>((resolve, reject) => {
      this.requests.postRequest(
        BackendAdminRoutes.GetCheats,
        user,
        (answer: ServerAnswer) => {
          if (answer.status) {
            resolve(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }
  public addCheat(user: User, item: Item, amount: number) {
    return new Promise<Cheat>((resolve, reject) => {
      this.requests.postRequest(
        BackendAdminRoutes.AddCheat,
        {
          item_id: item.id,
          user_id: user.id,
          amount:  amount
        },
        (answer: ServerAnswer) => {
          if (answer.status) {
            resolve(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }
  public deleteCheat(cheat: Cheat) {
    this.requests.postRequest(
      BackendAdminRoutes.DeleteCheat,
      cheat
    );
  }

  public getUserAddress(user: User) {
    return new Promise<UserAddress>((resolve, reject) => {
      this.requests.postRequest(
        BackendAdminRoutes.GetUserAddress,
        user,
        (answer: ServerAnswer) => {
          if (answer.status) {
            resolve(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }
  public getUserStat(user: User) {
    return new Promise<AllUserStat>((resolve, reject) => {
      this.requests.postRequest(
        BackendAdminRoutes.GetUserStat,
        user,
        (answer: ServerAnswer) => {
          if (answer.status) {
            resolve(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }

  //  Сортировка по убыванию баланса пузырьком
  private sortByBalance(users: Array<User>): Array<User> {
    for (let i = 0; i < users.length; i++) {
      for (let g = users.length - 1; g > i; g--) {
        if (users[g - 1].balance < users[g].balance) {
          const x = users[g - 1];
          users[g - 1] = users[g];
          users[g] = x;
        }
      }
    }
    return users;
  }
  //  Сортировка сначала админы, потом юзеры
  private sortByRights(users: Array<User>): Array<User> {
    for (let i = 0; i < users.length; i++) {
      for (let g = users.length - 1; g > i; g--) {
        if (users[g].is_admin) {
          const x = users[g - 1];
          users[g - 1] = users[g];
          users[g] = x;
        }
      }
    }
    return users;
  }

  get usersList() { return this.users; }
}
