import { Injectable } from '@angular/core';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {Box} from '../../models/box';
import {BackendRoutes} from '../../routing/pathes';

@Injectable()
export class BoxesService {
  private boxes: Array<Box>;
  private _allBoxes: Array<Box>;
  private _priorityBoxes: Array<Box>;
  private inited: boolean;

  constructor(
    private requests: RequestsService
  ) {
    this.boxes = [];
    this._allBoxes = [];
    this._priorityBoxes = [];
    this.inited = false;

    this.requests.getRequest(
      BackendRoutes.GetBoxes,
      (answer: ServerAnswer) => {
        this._allBoxes = answer.data;

        for (let i = 0; i < answer.data.length; i++) {
          if (!answer.data[i].is_priority) {
            this.boxes.push(answer.data[i]);
          } else {
            this._priorityBoxes.push(answer.data[i]);
          }
        }

        this.inited = true;
      });
  }

  get allBoxes() { return this._allBoxes; }
  get boxesList() { return this.boxes; }
  get priorityBoxes() { return this._priorityBoxes; }
  get isInited()  { return this.inited; }
}
