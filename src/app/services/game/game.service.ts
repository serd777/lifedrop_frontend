import { Injectable } from '@angular/core';
import {Box} from '../../models/box';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {BoxItem} from '../../models/box_item';
import {BackendRoutes} from '../../routing/pathes';
import {Drop} from '../../models/drop';
import {Item} from '../../models/item';
import {UserService} from '../user/user.service';
import {OwnBox} from '../../models/own_box';
import {OwnBoxItem} from '../../models/own_box_item';

@Injectable()
export class GameService {
  private _ownBox: OwnBox;
  private _box: Box;
  private _prize: Drop;
  private _boxSelected: boolean;
  private _gameStarted: boolean;
  private _prizeFromGame: boolean;

  private _roulleteItems: Array<Item>;
  private _items: Array<BoxItem>|Array<OwnBoxItem>;

  constructor(
    private requests: RequestsService,
    private userService: UserService
  ) {
    this._boxSelected = false;
    this._gameStarted = false;
    this._prizeFromGame = false;

    this._items = [];
    this._roulleteItems = [];

    this._box = new Box();
    this._prize = new Drop();
    this._ownBox = new OwnBox();
  }

  public openBox(box: Box) {
    return new Promise<any>((resolve, reject) => {
      this.shuffleRoullete();
      this.requests.postRequest(
        BackendRoutes.OpenBox,
        box,
        (answer: ServerAnswer) => {
          if (answer.status) {
            this._prize = answer.data;
            this._prize.status = 'Unchecked';
            this._prizeFromGame = true;
            this._roulleteItems[28] = this._prize.item;
            this.userService.incGamesPlayed();

            resolve(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }
  public openOwnBox(box: OwnBox) {
    return new Promise<any>((resolve, reject) => {
      this.shuffleRoullete();
      this.requests.postRequest(
        BackendRoutes.OpenOwnBox,
        box,
        (answer: ServerAnswer) => {
          if (answer.status) {
            this._prize = answer.data;
            this._prize.status = 'Unchecked';
            this._roulleteItems[28] = this._prize.item;
            this._prizeFromGame = true;
            this.userService.incGamesPlayed();

            resolve(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }
  private shuffleRoullete() {
    for (let i = this._roulleteItems.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [this._roulleteItems[i], this._roulleteItems[j]] = [this._roulleteItems[j], this._roulleteItems[i]];
    }
  }

  public sellItem(drop: Drop) {
    this.requests.postRequest(
      BackendRoutes.SellItem,
      drop,
      (answer: ServerAnswer) => {
        this.userService.user.balance += drop.item.price;
        this.userService.addProfit(drop.item.price);
        drop.status = 'Sold';
      });
  }
  public takeItem(drop: Drop) {
    this.requests.postRequest(
      BackendRoutes.TakeItem,
      drop,
      (answer: ServerAnswer) => {
        if (answer.status) {
          drop.status = 'Waiting';
        }
      }
    );
  }

  get box() { return this._box; }
  set box(value: Box) {
    this._box = value;

    this.requests.postRequest(
      BackendRoutes.GetBoxItems,
      value,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.sortItems(answer.data);
          this._roulleteItems = [];
          if (this._items.length) {
            this.generateRoulleteItems(this._items);
          }

          this._boxSelected = true;
        }
      });
  }

  get ownBox() { return this._ownBox; }
  set ownBox(value: OwnBox) {
    this._ownBox = value;

    this.requests.postRequest(
      BackendRoutes.GetOwnBoxesItems,
      value,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this.sortItems(answer.data);
          this._roulleteItems = [];
          if (this._items.length) {
            this.generateRoulleteItems(this._items);
          }

          this._boxSelected = true;
        }
      });
  }

  get isBoxSelected() { return this._boxSelected; }
  get isGameStarted() { return this._gameStarted; }
  get isPrizeFromGame() { return this._prizeFromGame; }

  get prize() { return this._prize; }
  get items() { return this._items; }
  get roulleteItems() { return this._roulleteItems; }
  set gameStarted(value: boolean) { this._gameStarted = value; }
  set prize(value: Drop) { this._prize = value; this._prizeFromGame = false; }

  private generateRoulleteItems(arr) {
    const length = (32 / arr.length) + 1;

    for (let i = 0; i < length; i++) {
      for (const item of arr) {
        this._roulleteItems.push(item.item);
      }
    }
  }

  private sortItems(items: Array<BoxItem>|Array<OwnBoxItem>) {
    let f = true;

    while (f) {
      f = false;

      for (let i = 0; i < items.length - 1; i++) {
        if (items[i + 1].item.price > items[i].item.price) {
          const buf = items[i + 1];

          items[i + 1] = items[i];
          items[i] = buf;

          f = true;
        }
      }
    }

    this._items = items;
  }
}
