import { TestBed, inject } from '@angular/core/testing';

import { LiveDropService } from './live-drop.service';

describe('LiveDropService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LiveDropService]
    });
  });

  it('should be created', inject([LiveDropService], (service: LiveDropService) => {
    expect(service).toBeTruthy();
  }));
});
