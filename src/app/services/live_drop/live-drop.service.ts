import { Injectable } from '@angular/core';
import {WebSocketService} from '../websocket/web-socket.service';
import {Drop} from '../../models/drop';

@Injectable()
export class LiveDropService {
  private _drops: Array<Drop>;

  constructor(
    private webSocket: WebSocketService
  ) {
    this._drops = [];

    this.webSocket.onEvent('live_drop_items')
      .subscribe((drops: Array<Drop>) => this._drops = drops.reverse());
    this.webSocket.onEvent('drop_added')
      .subscribe((drop: Drop) => this._drops.unshift(drop));
  }

  get drops() { return this._drops; }
}
