import { Injectable } from '@angular/core';

export const ModalNames = {
  HiddenAll: '',
  DonateModal: 'donate_modal',
  AddressModal: 'address_modal',
  PromoCodeModal: 'promo_code_modal',
  PrizeModal: 'prize_modal',
  TrackCodeModal: 'track_code_modal',
};

@Injectable()
export class ModalsService {
  private _current: string;

  constructor() {
    this._current = ModalNames.HiddenAll;
  }
  public showModal(name: string) {
    this._current = name;
  }

  get currentModal() { return this._current; }

  public getDonateModalName()    { return ModalNames.DonateModal; }
  public getAddressModalName()   { return ModalNames.AddressModal; }
  public getPromoCodeModalName() { return ModalNames.PromoCodeModal; }
  public getPrizeModalName()     { return ModalNames.PrizeModal; }
  public getTrackCodeModalName() { return ModalNames.TrackCodeModal; }

  public isHiddenAll()      { return this._current === ModalNames.HiddenAll; }
  public isDonateModal()    { return this._current === this.getDonateModalName(); }
  public isAddressModal()   { return this._current === this.getAddressModalName(); }
  public isPromoCodeModal() { return this._current === this.getPromoCodeModalName(); }
  public isPrizeModal()     { return this._current === this.getPrizeModalName(); }
  public isTrackCodeModal() { return this._current === this.getTrackCodeModalName(); }

  public hideAll() { this._current = ModalNames.HiddenAll; }
}
