import { Injectable } from '@angular/core';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {PromoOffer} from '../../models/promo_offer';
import {BackendRoutes} from '../../routing/pathes';

@Injectable()
export class OffersService {
  private offers: Array<PromoOffer>;

  constructor(
    private requests: RequestsService
  ) {
    this.offers = [];
    this.requests.getRequest(
      BackendRoutes.GetOffers,
      (answer: ServerAnswer) => {
        this.offers = answer.data;
    });
  }

  get offersList() { return this.offers; }
}
