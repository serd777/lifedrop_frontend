import { TestBed, inject } from '@angular/core/testing';

import { OwnBoxesService } from './own-boxes.service';

describe('OwnBoxesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OwnBoxesService]
    });
  });

  it('should be created', inject([OwnBoxesService], (service: OwnBoxesService) => {
    expect(service).toBeTruthy();
  }));
});
