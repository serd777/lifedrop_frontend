import { Injectable } from '@angular/core';
import {OwnBoxImg} from '../../models/own_box_img';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {BackendRoutes} from '../../routing/pathes';
import {OwnBoxAvailItem} from '../../models/own_box_avail_item';
import {OwnBox} from '../../models/own_box';
import {OwnBoxStat} from '../../models/own_box_stat';
import {UserService} from '../user/user.service';

@Injectable()
export class OwnBoxesService {
  private _initCount: number;

  private _imges: Array<OwnBoxImg>;
  private _items: Array<OwnBoxAvailItem>;
  private _boxes: Array<OwnBox>;
  private _stats: Array<OwnBoxStat>;
  private _topBoxes: Array<OwnBox>;

  private amountService = 3;

  constructor(
    private requests: RequestsService,
    private userService: UserService
  ) {
    this._imges = [];
    this._items = [];
    this._stats = [];
    this._topBoxes = [];
    this._initCount = 0;

    this.getImgs();
    this.getItems();
    this.getBoxesStat();
  }

  private getImgs() {
    this.requests.getRequest(
      BackendRoutes.GetOwnBoxImgs,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._imges = answer.data;
          this._initCount++;
        }
      });
  }
  private getItems() {
    this.requests.getRequest(
      BackendRoutes.GetItems,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._items = answer.data;
          this.sortItemsByPrice();
          this._initCount++;
        }
      });
  }
  private getBoxesStat() {
    this.requests.getRequest(
      BackendRoutes.OwnBoxesStat,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._stats = answer.data;
          this.sortBoxStat();
          this.getBoxes();
        }
      });
  }
  private getBoxes() {
    if (!this.userService.isInited) {
      const that = this;
      setTimeout(() => { that.getBoxes(); }, 1500);
      return;
    }

    this.requests.getRequest(
      BackendRoutes.GetOwnBoxes,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._boxes = answer.data;
          this.initTopBoxes();
          this.sortByUser();
          this._initCount++;
        }
      });
  }
  private initTopBoxes() {
    for (const stat of this._stats) {
      const box = this._boxes.filter((value: OwnBox) => value.id === stat.box_id)[0];
      this._topBoxes.push(box);

      if (this._topBoxes.length === 8) {
        break;
      }
    }
  }

  private sortItemsByPrice() {
    for (let i = 0; i < this._items.length; i++) {
      for (let g = this._items.length - 1; g > i; g--) {
        if (this._items[g - 1].item.price < this._items[g].item.price) {
          const x = this._items[g - 1];
          this._items[g - 1] = this._items[g];
          this._items[g] = x;
        }
      }
    }
  }
  private sortByUser() {
    if (!this._boxes.length) {
      return;
    }

    if (this.userService.isAuth) {
      let index = 0;
      const length = this._boxes.length - 1;

      while (
        this._boxes[index].user_id === this.userService.user.id &&
        index < length
      ) {
        index++;
      }

      for (let i = length; i >= index; i--) {
        if (this._boxes[i].user_id === this.userService.user.id) {
          const buf = this._boxes[index];
          this._boxes[index] = this._boxes[i];
          this._boxes[i] = buf;

          while (
            this._boxes[index].user_id === this.userService.user.id &&
            index < length
          ) {
            index++;
          }
        }
      }
    }
  }
  private sortBoxStat() {
    for (let i = 0; i < this._stats.length; i++) {
      for (let g = this._stats.length - 1; g > i; g--) {
        if (this._stats[g - 1].amount < this._stats[g].amount) {
          const x = this._stats[g - 1];
          this._stats[g - 1] = this._stats[g];
          this._stats[g] = x;
        }
      }
    }
  }

  public createBox(
    name: string,
    items: Array<OwnBoxAvailItem>,
    img: OwnBoxImg
  ) {
    return new Promise<OwnBox>((resolve, reject) => {
      this.requests.postRequest(
        BackendRoutes.CreateOwnBox,
        {name: name, items: items, img_id: img.id},
        (answer: ServerAnswer) => {
          if (answer.status) {
            resolve(answer.data);
            this._boxes.unshift(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }

  get boxesImages() { return this._imges; }
  get items() { return this._items; }
  get boxes() { return this._boxes; }
  get topBoxes() { return this._topBoxes; }

  get isInited() { return this._initCount === this.amountService; }
}
