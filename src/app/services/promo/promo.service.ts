import { Injectable } from '@angular/core';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {BackendRoutes} from '../../routing/pathes';

@Injectable()
export class PromoService {

  constructor(
    private requests: RequestsService
  ) { }

  activatePromo(code: string) {
    return new Promise<number>((resolve, reject) => {
      this.requests.postRequest(
        BackendRoutes.ActivatePromo,
        {code: code},
        (answer: ServerAnswer) => {
          if (!answer.status) {
            reject(0);
          } else {
            resolve(answer.data);
          }
        });
    });
  }
}
