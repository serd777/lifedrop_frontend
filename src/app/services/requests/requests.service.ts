import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BackendRoutes} from '../../routing/pathes';

export interface ServerAnswer {
  status: boolean;
  data: any;
}

//  Сервис для отправки запросов к Backend
@Injectable()
export class RequestsService {
  private token: string;
  private options: object;

  constructor(
    public http: HttpClient
  ) {
    this.options = {withCredentials: true};
    this.getToken();
  }

  //  Метод получения CSRF токена
  private getToken() {
    this.http.get(BackendRoutes.GetCsrfToken, this.options)
      .subscribe((answer: ServerAnswer) => {
        this.token = answer.data;
        this.options = {
          headers : new HttpHeaders({
            'Content-Type'  : 'application/json',
            'X-CSRF-TOKEN'  : this.token
          }),
          withCredentials : true
        };
      });
  }

  //  Post Request
  public postRequest(url: string, data: object = {}, callback = ((answer) => {})) {
    this.http.post<ServerAnswer>(url, data, this.options)
      .subscribe(callback);
  }

  //  Get Request
  public getRequest(url: string, callback = ((answer) => {})) {
    this.http.get<ServerAnswer>(url, this.options)
      .subscribe(callback);
  }
}
