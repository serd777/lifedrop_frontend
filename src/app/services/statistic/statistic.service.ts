import { Injectable } from '@angular/core';
import {WebSocketService} from '../websocket/web-socket.service';
import {TopUser} from '../../models/top_user';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {BackendRoutes} from '../../routing/pathes';

@Injectable()
export class StatisticService {
  private _online: number;
  private _total_users: number;
  private _games_played: number;
  private _top_users: Array<TopUser>;

  constructor(
    private webSocket: WebSocketService,
    private requests: RequestsService
  ) {
    this._online = 0;
    this._total_users = 0;
    this._games_played = 0;

    this._top_users = [];
    this.updateTopUsers();

    this.webSocket.onEvent('total_users_updated')
      .subscribe((amount: number) => (this._total_users = amount));
    this.webSocket.onEvent('games_updated')
      .subscribe((amount: number) => (this._games_played = amount));
    this.webSocket.onEvent('online_updated')
      .subscribe((amount: number) => (this._online = amount));
  }

  private updateTopUsers() {
    this.requests.getRequest(
      BackendRoutes.GetTopUsers,
      (answer: ServerAnswer) => {
        if (answer.status) {
          this._top_users = answer.data;
        }
      });
  }

  get totalUsers() { return this._total_users; }
  get online() { return this._online; }
  get gamesPlayed() { return this._games_played; }
  get topUsers() { return this._top_users; }
}
