import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {BackendRoutes} from '../../routing/pathes';
import {UserAddress} from '../../models/user_address';
import {Drop} from '../../models/drop';

@Injectable()
export class UserService {
  private currentUser: User;
  private _userAddress: UserAddress;
  private _userDrops: Array<Drop>;
  private _gamesPlayed: number;
  private _userProfit: number;
  private _referrals: number;

  private authenticated: boolean;
  private inited: boolean;

  constructor(
    private requests: RequestsService
  ) {
    this.authenticated = false;
    this.inited = false;

    this.currentUser = new User();
    this._userAddress = new UserAddress();
    this._userDrops = [];
    this._gamesPlayed = 0;
    this._userProfit = 0;
    this._referrals = 0;

    this.initService();
  }

  private initService() {
    this.requests.getRequest(BackendRoutes.GetUserInfo, (answer: ServerAnswer) => {
      if (answer.status) {
        this.currentUser  = answer.data.user;
        this._userAddress = answer.data.address;
        this._userDrops   = answer.data.drops;
        this._gamesPlayed = answer.data.games_played;
        this._userProfit  = answer.data.profit;
        this._referrals   = answer.data.referrals;

        this.authenticated = true;
      }
      this.inited = true;
    });
  }
  public updateAddress() {
    return new Promise<ServerAnswer>((resolve, reject) => {
      this.requests.postRequest(
        BackendRoutes.UpdateAddress,
        this._userAddress,
        (answer: ServerAnswer) => {
          if (answer.status) {
            resolve(answer);
          } else {
            reject(answer);
          }
        });
    });
  }
  public getUserProfile(id: number) {
    return new Promise<any>((resolve, reject) => {
      this.requests.postRequest(
        BackendRoutes.GetUserProfile,
        {id: id},
        (answer: ServerAnswer) => {
          if (answer.status) {
            resolve(answer.data);
          } else {
            reject(answer.data);
          }
        });
    });
  }

  public incGamesPlayed()    { this._gamesPlayed++; }
  public addDrop(drop: Drop) { this._userDrops.unshift(drop); }
  public addProfit(amount: number) { this._userProfit += amount; }

  get isInited() { return this.inited; }
  get isAuth()   { return this.authenticated; }
  get isBanned() { return this.currentUser.is_blocked; }
  get isAdmin()  { return this.currentUser.is_admin; }

  get user()        { return this.currentUser; }
  get userAddress() { return this._userAddress; }
  get userDrops()   { return this._userDrops; }
  get gamesPlayed() { return this._gamesPlayed; }
  get userProfit()  { return this._userProfit; }
  get referralsCount() { return this._referrals; }
}
