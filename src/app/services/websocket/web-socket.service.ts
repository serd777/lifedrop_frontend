import { Injectable } from '@angular/core';
import * as socketIO from 'socket.io-client';
import {Observable} from 'rxjs/Observable';
import {FrontendHost} from '../../routing/pathes';

@Injectable()
export class WebSocketService {
  private server: SocketIOClient.Socket;

  constructor() {
    this.server = socketIO(
      FrontendHost + ':7030', {
        secure: true,
      }
    );
  }

  sendRequest(event: string, args: any = {}) {
    this.server.emit(event, args);
  }

  onEvent(event: string) {
    return new Observable<any>(observer => {
      this.server.on(event, (data) => {
        observer.next(data);
      });
    });
  }
}
